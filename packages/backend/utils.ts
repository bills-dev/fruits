export type Class<T> = new(...args: any[]) => T;

export class Utils {

  static guid() {
    function s4() {
      return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
    }

    return `${s4()}${s4()}${s4()}${s4()}${s4()}${s4()}${s4()}${s4()}`;
  }

  static qsParse(qs: string) {
    if (qs[0] === '?') {
      qs = qs.substring(1);
    }
    return qs.split('&').reduce((p, c) => {
      const item = c.split('=');
      let value: any = item[1];
      if (value === 'true') {
        value = true;
      } else if (value === 'false') {
        value = false;
      }
      p[item[0]] = decodeURI(value);
      return p;
    }, {});
  }

  static qsStringify(obj: { [ k: string ]: number | string | boolean }) {
    const str = [];
    for (const p in obj) {
      str.push(encodeURIComponent(p) + '=' + encodeURIComponent(String(obj[p])));
    }
    return str.join('&');
  }

  static async wait(seconds: number): Promise<any> {
    return new Promise((accept) => {
      setTimeout(accept, seconds * 1000);
    }) as Promise<any>;
  }

  static ucFirst(str: string = '') {
    return str.charAt(0) && str.charAt(0).toUpperCase() + str.slice(1);
  }
}
