import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { WsAdapter } from '@nestjs/websockets';
import { AppService } from "./app.service";
import * as cors     from "cors"

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.useWebSocketAdapter(new WsAdapter(app));
  const service: AppService = app.get(AppService);
  await service.start();
  app.use(cors());
  await app.listen(3000);
}

bootstrap();
