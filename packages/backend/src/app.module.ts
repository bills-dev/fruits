import { Module } from '@nestjs/common';
import { AppService } from './app.service';
import { EventsModule } from './events/events.module';
import { MessagesModule } from "./messages/messages.module";
import { DeviceModule } from "./device/device.module";
import { NotificationModule } from "./notification/notification.module";
import {SnapshotsModule} from "./snapshots/snapshots.module";


@Module({
  imports: [EventsModule, MessagesModule, DeviceModule, NotificationModule, SnapshotsModule],
  providers: [AppService],
})
export class AppModule {
}
