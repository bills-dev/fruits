import * as webpush            from "web-push";
import { PushSubscription }    from "web-push";
import { Get }                 from '@nestjs/common';
import { Post }                from '@nestjs/common';
import { Body }                from '@nestjs/common';
import { Inject }              from '@nestjs/common';
import { Controller }          from '@nestjs/common';
import { NotificationService } from "./notification.service";

@Controller('api/notification')
export class ApiNotificationController {

  private notificationsData: any = [];

  private subscribers: any = {};

  private privateKey: string = "EWgN_hjxkZETo534hZdKI52qqWjegC2ldKQDke6KYNM";

  private publicKey: string = "BFl7O-Ml0GCtTzDQ59-LKi3wovUsl_s2hcWld-baxWDAWj0SbjHIcEzcmGLYi2eklHpi0bxsW1JyTFICmhaywLc";


  @Inject()
  private readonly notifications: NotificationService;

  @Get('subscribers')
  getAll() {
    return this.subscribers;
  }

  @Post('subscribe')
  subscribe(@Body() data: ISubscribe) {
    const {
      deviceId,
      subscription,
    } = data;

    this.subscribers = {
      ...this.subscribers,
      [deviceId]: {
        ...subscription,
        deviceId: deviceId,
      }
    };

    return {
      message: "Subscribed"
    };
  }

  @Get('get-public-key')
  getPublicKey() {
    return {
      publicKey: this.publicKey
    };
  }

  @Post('send')
  async sendNotification(@Body() nf: any) {
    try {
      if (!nf.deviceId) {
        return {
          message: "Device not found",
          error  : true,
        }
      }

      const subscriber = this.subscribers[nf.deviceId];

      if (!subscriber) {
        return {
          message: "User not found",
          error  : true,
        }
      }

      const pushSubscription = {
        endpoint: subscriber.endpoint,
        keys    : {
          auth  : subscriber.keys.auth,
          p256dh: subscriber.keys.p256dh,
        }
      };
      const nfData = {
        title: nf.title,
        icon : nf.iconUrl,
        body : nf.message,
        tag  : nf.deviceId,
        image: nf.imageUrl,
        data : {
          launchUrl: nf.launchUrl
        }
      };

      const payload = JSON.stringify(nfData);

      const options = {
        vapidDetails: {
          subject   : 'mailto:example@example.org',
          publicKey : this.publicKey,
          privateKey: this.privateKey
        }
      };

      await webpush.sendNotification(
        pushSubscription,
        payload,
        options
      );

      if (this.notificationsData[nf.deviceId]) {
        this.notificationsData = {
          ...this.notificationsData,
          [nf.deviceId]: [
            ...this.notificationsData[nf.deviceId],
            nfData,
          ]
        };
      } else {
        this.notificationsData = {
          ...this.notificationsData,
          [nf.deviceId]: [
            nfData,
          ]
        };
      }

      return {
        message: "Success",
        error  : false,
      }
    } catch (e) {
      return {
        message: e.message,
        error  : true,
      }
    }
  }

  @Post('close')
  nfClosed(@Body() closeAction: any) {
    return {
      message: "Success"
    };
  }

  @Post('get-messages')
  getMessages(@Body() body: any) {
    if (!this.notificationsData[body.deviceId]) {
      return {
        data: []
      };
    }

    return {
      data: this.notificationsData[body.deviceId]
    };
  }
}

export interface ISubscribe {
  deviceId: string,
  subscription: PushSubscription,
}
