import { Module } from '@nestjs/common';
import { ApiNotificationController } from "./api.notification.controller";
import { NotificationService } from "./notification.service";

@Module({
  controllers: [ApiNotificationController],
  providers: [NotificationService],
  exports: [NotificationService],
})
export class NotificationModule {
}
