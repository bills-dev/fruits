export abstract class Signal<T extends Function> extends Function {
  abstract attach(callback: T): T;

  abstract detach(callback: T): T;

  abstract detachAll();
}

function getSignal() {
  let s;
  const handlers = new Set<Function>();
  s = function (...args) {
    for (const handler of handlers) {
      handler(...args);
    }
  };
  Object.defineProperties(s, {
    attach: {
      value(handler: Function) {
        handlers.add(handler);
        return handler;
      },
    },
    detach: {
      value(handler: Function) {
        handlers.delete(handler);
        return handler;
      },
    },
    detachAll: {
      value() {
        handlers.clear();
      },
    },
  });
  return s;
}

export function signal(target: any, key: string): any {
  if (typeof target === 'function') {
    const s = getSignal();
    return {
      value: s,
    };
  } else {
    return {
      get() {
        const s = getSignal();
        Object.defineProperty(this, key, {
          value: s,
        });
        return s;
      },
    };
  }

}
