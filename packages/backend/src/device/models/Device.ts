import { ISession } from "../../session/models/Session";

export interface IDevice {
  id: string;
  pushNotifications?: boolean;
  sessions?: ISession[]
}