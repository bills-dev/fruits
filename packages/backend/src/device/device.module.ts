import { Module } from '@nestjs/common';
import { ApiDeviceController } from "./api.device.controller";
import { DeviceService } from "./device.service";
import { SessionModule } from "../session/session.module";

@Module({
  imports: [SessionModule],
  controllers: [ApiDeviceController],
  providers: [DeviceService],
  exports: [DeviceService],
})
export class DeviceModule {
}
