import { Body, Controller, Get, Inject, NotFoundException, Param, Patch, UseGuards } from '@nestjs/common';
import { DeviceService } from "./device.service";

@Controller('api/device')
//@UseGuards(AuthGuard())
export class ApiDeviceController {

  @Inject()
  private readonly devices: DeviceService;

  @Get()
  getAll() {
    return this.devices.getDevices();
  }

  @Get(':id')
  getById(@Param('id') id) {
    return this.devices.getDevice(id);
  }
}
