import { Inject, Injectable } from '@nestjs/common';
import { SessionService } from "../session/session.service";
import { IDevice } from "./models/Device";
import { ISession } from "../session/models/Session";

@Injectable()
export class DeviceService {

  @Inject()
  public sessions: SessionService;

  private devices: Map<string, IDevice> = new Map();

  public async start() {
    await this.sessions.start();
    console.info('STARTED DeviceService')
  }

  public addDevice(data: IDevice): IDevice {
    let device = this.getDevice(data.id);
    if (!device) {
      this.devices.set(data.id, data);
      device = data;
    }
    return device;
  }

  public getDevice(id: string) {
    return this.devices.get(id);
  }

  public addSession(session: ISession) {
    const device = this.getDevice(session.deviceId);
    if (device) {
      if (!device.sessions) {
        device.sessions = [];
      }
      device.sessions.push(session);
    }
    this.sessions.addSession(session);
  }

  public getDevices(): IDevice[] {
    return Array.from(this.devices.values())
  }
}
