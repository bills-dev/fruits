import { Inject, Injectable } from '@nestjs/common';
import { MessagesService } from "./messages/messages.service";
import { DeviceService } from "./device/device.service";
import { NotificationService } from "./notification/notification.service";

@Injectable()
export class AppService {

  @Inject()
  private readonly messages: MessagesService;
  @Inject()
  private readonly devices: DeviceService;
  @Inject()
  private readonly notifications: NotificationService;
  public async start() {
    console.info('STARTING APP');
    await this.devices.start();
    await this.notifications.start();
    await this.messages.start();
    console.info('APP STARTED');
  }
}
