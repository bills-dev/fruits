import { HttpClient, HttpClientOptions } from "../HttpClient";

enum FbApiConfig {
  PageAccessToken = 'EAAHfmXG7LS8BAEZAmxiS15gRK1Cwqmrxqgh72AcCSsvGOyo7r4kTH5djBSa2hfZBfJ2zZBZACEHGfPBzDxda5UnDKtdqxivZBNZBzZBOWeZClBiFCi8zfnXGYRif0It5namkhvZBiFGuB4AOPvdsZCHXAYEiDm6ZBGr2lIjXhv90JWlcAZDZD',
  Host = 'graph.facebook.com',
  Version = 'v2.6',
  Path = 'me/messages'
}

export class FbApi extends HttpClient {

  async request(opts: HttpClientOptions) {
    opts.headers = Object.assign(opts.headers || {}, {
      'Content-Type': 'application/json'
    });
    opts.body = Object.assign(opts.body || {}, {
      'access_token': FbApiConfig.PageAccessToken
    });
    opts.hostname = FbApiConfig.Host;
    opts.protocol = 'https:';
    const res = await super.request(opts);
    if (res.headers['content-type'].includes('text')){
      try {
        res.body = JSON.parse(res.body);
      } catch (e){}
    }
    return res;
  }

  async callSendAPI(messageData = {}) {
    try {
      await this.post(`/${FbApiConfig.Version}/${FbApiConfig.Path}`, void 0, messageData);
    } catch (e) {
      console.error(e)
    }
  }

  async getUserNameByIdAPI(id = null) {
    try {
      let response = await this.get(`/${id}?fields=id,name&access_token=${FbApiConfig.PageAccessToken}`);
      return response.body
    } catch (e) {
      console.error(e)
    }
  }
}