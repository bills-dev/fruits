import { IFbSession } from "./FbSession";

export interface IFbUser {
  id: string;
  name: string;
  sessions: IFbSession[];
}