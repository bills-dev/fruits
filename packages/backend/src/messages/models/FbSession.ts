export enum DebtPurpose {
  CreditCardRefinancing = 'Credit Card Refinancing',
  DebtConsolidation = 'Debt Consolidation',
  HomeImprovement = 'Home Improvement',
  MajorPurchase = 'Major Purchase',
  Wedding = 'Wedding',
  TravelOrVacation = 'Travel/Vacation',
  BusinessExpenses = 'Business Expenses',
  AutoPurchase = 'Auto Purchase',
  MedicalExpenses = 'Medical Expenses',
  MovingExpenses = 'Moving Expenses',
  Other = 'Other',
}

export enum Interested {
  No = 'Yes',
  Yes = 'No',
}

export enum CreditScore {
  Excellent = 'Excellent (740+)',
  VeryGood = 'Very Good (700-739)',
  Good = 'Good (670-699)',
  Fair = 'Fair (640-669)',
  Poor = 'Poor (639 or less)',
}

export enum QuestionId {
  interested = 'interested',
  debt = 'debt',
  purpose = 'purpose',
  creditScore = 'creditScore',
}

export const QUESTION_IDS = [
  QuestionId.interested,
  QuestionId.debt,
  QuestionId.purpose,
  QuestionId.creditScore,
];

export interface IFbSession {
  id: string;
  fbUserId: string;
  active: boolean;
  landed?: boolean;
  lastQuestionId?: QuestionId;
  formData: {
    name?: string;
    debt?: number;
    purpose?: DebtPurpose;
    creditScore?: CreditScore;
  };
  redirectUrl: string
}