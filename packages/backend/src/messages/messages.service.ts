import { Inject, Injectable } from '@nestjs/common';
import { FbApi } from "./FbApi";
import { IFbUser } from "./models/FbUser";
import { Utils } from "../../utils";
import {QuestionId, QUESTION_IDS, DebtPurpose, CreditScore, Interested} from "./models/FbSession";

const FORM_QUESTIONS = {
  [QuestionId.interested] : 'Are you interested in borrowing money?',
  [QuestionId.debt] : 'How much would you like to borrow?',
  [QuestionId.purpose] : 'How do you plan to use the money?',
  [QuestionId.creditScore] : 'How is your credit?',
};
const env = process.env.NODE_ENV;

@Injectable()
export class MessagesService {

  @Inject()
  private readonly fbApi: FbApi;

  private fbUsers: Map<string, IFbUser> = new Map();

  public async start() {
    console.info('STARTED MessagesService')
  }

  public getFbUsers(){
    return Array.from(this.fbUsers.values());
  }

  public getFbSession(id: string) {
    for (const user of this.getFbUsers()) {
      const session = user.sessions.find(u => u.id === id);
      if (session) {
        return session;
      }
    }
  }

  private sendPurposeQuestion(id, qId) {
      return this.sendMessage(id, FORM_QUESTIONS[qId], Object.values(DebtPurpose).map((v) => {
          return {
              "content_type":"text",
              "title":v.replace(/_/g, ' '),
              "payload":v
          }
      }));
  }
  private sendInterestedQuestion(id, qId) {
      return this.sendMessage(id, FORM_QUESTIONS[qId], Object.values(Interested).map((v) => {
          return {
              "content_type":"text",
              "title":v.replace(/_/g, ' '),
              "payload":v
          }
      }));
  }
  private sendCreditQuestion(id, qId) {
      return this.sendMessage(id, FORM_QUESTIONS[qId], Object.values(CreditScore).map((v) => {
          return {
              "content_type":"text",
              "title":v.replace(/_/g, ' '),
              "payload":v
          }
      }));
  }

  private async upsertUser(id: string) {
    let user = this.fbUsers.get(id);
    if (!user) {
      const {name} = await this.fbApi.getUserNameByIdAPI(id);
      user = {
        id,
        name,
        sessions: [],
      };
      this.fbUsers.set(id, user);
    }
    return user;
  }

  public async receivedMessage(id, text) {
    const fbUser = await this.upsertUser(id);
    let lastSession = fbUser.sessions[fbUser.sessions.length - 1];
    const redirectUrlHost = env === 'local'?'http://localhost:3001':'https://flow.fruits.ffn.li';
    switch (text.replace(/[^\w\s]/gi, '').trim().toLowerCase()) {
        case 'good morning':
            await this.sendMessage(fbUser.id, `Good Morning dear customer`);
          return;
      case 'hello':
      case 'hi':
        if (lastSession) {
          lastSession.active = false;
        }
        const id = Utils.guid();
        const session = {
          id,
          fbUserId: fbUser.id,
          active: true,
          formData: {
            name: fbUser.name,
          },
          redirectUrl: `${redirectUrlHost}?ref=${id}&utm_campaign=facebook`,
        };
        fbUser.sessions.push(session);
        lastSession = session;
        await this.sendMessage(fbUser.id, `Welcome ${fbUser.name}!\nThanks for connecting us!`);
        break;
    }
    if (lastSession && lastSession.active) {
      const lastId: QuestionId = lastSession.lastQuestionId;
      let qId: QuestionId;
      if (!lastId) {
        qId = QUESTION_IDS[0];
      } else {
        switch (lastId) {
          case QuestionId.interested:
              if(Object.values(Interested).includes(text)){
                  if(text.toLowerCase() === 'no'){
                      await this.sendMessage(lastSession.fbUserId, 'Okay! :)');
                      return;
                  }else {
                      lastSession.formData[lastId] = text;
                  }
              }else {
                  await this.sendMessage(lastSession.fbUserId, 'Please Answer Yes or No');
                  await this.sendInterestedQuestion(lastSession.fbUserId, lastId);
                  return;
              }
          break;
          case QuestionId.debt:
            if(!Number.isNaN(Number(text))){
              lastSession.formData[lastId] = Number(text);
            }else {
              await this.sendMessage(lastSession.fbUserId, 'Please Answer with Number');
              await this.sendMessage(lastSession.fbUserId, FORM_QUESTIONS[lastId]);
              return;
            }
          break;
          case QuestionId.purpose:
            if(Object.values(DebtPurpose).includes(text)){
              lastSession.formData[lastId] = text;
            }else {
                await this.sendMessage(lastSession.fbUserId, 'Please click on one of answers');
              await this.sendPurposeQuestion(lastSession.fbUserId, lastId);
              return;
            }

          break;
          case QuestionId.creditScore:
            if(Object.values(CreditScore).includes(text)){
              lastSession.formData[lastId] = text;
            }else {
              await this.sendMessage(lastSession.fbUserId, 'Please click on one of answers');
                await this.sendCreditQuestion(lastSession.fbUserId, lastId);
              return;
            }
          break;
        }
        qId = QUESTION_IDS[QUESTION_IDS.indexOf(lastId)+1];
      }
      if(qId) {
        switch (qId) {
          case QuestionId.interested:
            await this.sendInterestedQuestion(lastSession.fbUserId, qId);
            break;
          case QuestionId.debt:
            await this.sendMessage(lastSession.fbUserId, FORM_QUESTIONS[qId]);
            break;
          case QuestionId.purpose:
              await this.sendPurposeQuestion(lastSession.fbUserId, qId);
            break;
          case QuestionId.creditScore:
              await this.sendCreditQuestion(lastSession.fbUserId, qId);
            break;
        }
        lastSession.lastQuestionId = qId;
      } else {
          await this.sendMessage(lastSession.fbUserId, 'Congratulations, you are prequalified for our loan!\nPlease click on the url to continue the full application.');
        await this.sendMessage(lastSession.fbUserId, lastSession.redirectUrl);
        lastSession.active = false;
      }

    } else {
      console.error('NO LAST SESSION');
    }
  }

  private sendMessage(id: string, text: string, quick_replies?: Array<object>) {
    return this.fbApi.callSendAPI({
      recipient: {
        id,
      },
      message: {
        text,
          quick_replies
      }
    });
  }
}
