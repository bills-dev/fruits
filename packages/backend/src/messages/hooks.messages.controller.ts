import { Body, Controller, Get, Inject, NotFoundException, Param, Patch, UseGuards, Query, ForbiddenException, Post } from '@nestjs/common';
import { MessagesService }                                                           from './messages.service';

@Controller('fb/webhook')
//@UseGuards(AuthGuard())
export class HooksMessagesController {

  @Inject()
  private readonly messages: MessagesService;


  @Get()
  getVerification(@Query('hub.verify_token') token, @Query('hub.challenge') challenge, @Query('hub.mode') mode) {
    let VERIFY_TOKEN = "FRUITS_VERIFY_TOKEN";
    if (mode && token) {
      if (mode === 'subscribe' && token === VERIFY_TOKEN) {
        return challenge
      }
    }
    throw new ForbiddenException()
  }


  @Post()
  postWebhook(@Body() data) {
    //console.log(JSON.stringify(data,null,2))
    if (data.object == 'page') {

      data.entry.forEach((pageEntry) => {
        let pageID = pageEntry.id;
        let timeOfEvent = pageEntry.time;

        pageEntry.messaging.forEach((messagingEvent) => {
          let senderId = messagingEvent.sender.id;
          let senderText = messagingEvent.message.text;
            if (messagingEvent.message) {
            let quick_reply;
            if (quick_reply = messagingEvent.message.quick_reply) {
                this.messages.receivedMessage(senderId,quick_reply.payload);
            } else {
                this.messages.receivedMessage(senderId,senderText);
            }
          }else {
            console.log("Webhook received unknown messagingEvent: ", messagingEvent);
          }
        });
      });
      return 'EVENT_RECEIVED'
    } else {
      throw new NotFoundException()
    }
  }
}
