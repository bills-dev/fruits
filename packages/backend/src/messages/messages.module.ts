import { Module } from '@nestjs/common';
import { MessagesService } from './messages.service';
import { ApiMessagesController } from "./api.messages.controller";
import { HooksMessagesController } from "./hooks.messages.controller";
import {FbApi} from "./FbApi";

@Module({
  controllers: [ApiMessagesController, HooksMessagesController],
  providers: [MessagesService, FbApi],
  exports: [MessagesService, FbApi],
})
export class MessagesModule {
}
