import {
  Body,
  Controller,
  Get,
  Post,
  Inject,
  NotFoundException,
  Query,
  ForbiddenException
} from '@nestjs/common';
import { MessagesService } from './messages.service';

@Controller('api/messages')

export class ApiMessagesController {

  @Inject()
  private readonly messages: MessagesService;

  @Get()
  getAll() {
    return this.messages.getFbUsers();
  }
}
