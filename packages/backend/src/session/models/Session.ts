import {IReduxAction} from "./ReduxAction";

export enum SessionEvents {
  AddSession = 'add-session',
  UpdateSession = 'update-session',
}

export interface ISession {
  id: string;
  fbRefId?: string;
  type: 'user' | 'admin' | 'watcher';
  deviceId?: string;
  active?: boolean;
  actions?: IReduxAction[];
  callStatus?:string;
  remoteSessionId?: string;
  wizard?: object;
}