export interface IReduxAction {
    timestamp: Date,
    action: {
        type: string,
        payload: any,
    },
}