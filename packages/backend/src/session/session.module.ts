import { Module } from '@nestjs/common';
import { ApiSessionController } from "./api.session.controller";
import { SessionService } from "./session.service";

@Module({
  controllers: [ApiSessionController],
  providers: [SessionService],
  exports: [SessionService],
})
export class SessionModule {
}
