import { Body, Controller, Get, Inject, NotFoundException, Param, Patch, UseGuards } from '@nestjs/common';
import { SessionService } from "./session.service";

@Controller('api/session')
//@UseGuards(AuthGuard())
export class ApiSessionController {

  @Inject()
  private readonly sessions: SessionService;

  @Get()
  getAll() {
    return this.sessions.getSessions();
  }

  @Get(':id')
  getById(@Param('id') id) {
      const session = this.sessions.getSession(id);
      if (session) {
        return session;
      }
      throw new NotFoundException()
  }
}
