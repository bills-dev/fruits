import { Inject, Injectable } from '@nestjs/common';
import { ISession } from "./models/Session";

@Injectable()
export class SessionService {

  private sessions: Map<string, ISession> = new Map();

  public async start() {
    console.info('STARTED SessionService')
  }

  public addSession(data: ISession): ISession {
    this.sessions.set(data.id, data);
    return data;
  }

  public getSession(id: string): ISession {
    return this.sessions.get(id);
  }

  public getSessions() {
    return Array.from(this.sessions.values());
  }
}
