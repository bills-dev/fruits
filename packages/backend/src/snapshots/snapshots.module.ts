import { Module } from '@nestjs/common';
import { ApiSnapshotsController } from "./api.snapshots.controller";
import {SnapshotsService} from "./snapshots.service";

@Module({
  controllers: [ApiSnapshotsController],
  providers: [SnapshotsService],
  exports: [SnapshotsService],
})
export class SnapshotsModule {
}
