
import {Get, Param, Res} from '@nestjs/common';
import { Post }                from '@nestjs/common';
import { Body }                from '@nestjs/common';
import { Inject }              from '@nestjs/common';
import { Controller }          from '@nestjs/common';
import { SnapshotsService } from "./snapshots.service";
import {Response} from "express";
import * as fs from "fs";

@Controller('api/snapshots')
export class ApiSnapshotsController {

  @Post()
  sendSnapshots(@Body() body: any, @Res() res: Response) {
    let directory = `./tmp/${body.sid}`;

    fs.access(directory,fs.constants.F_OK, (err)=>{
      if(err){
        console.log(`Directory ${directory} does not exist`)
        console.log(`There are no screenshots for this user`)
      } else {
          fs.readdir(directory, (err, files) => {
              if(err){
                  res.status(404);
              } else{
                  res.send(files);
              }
          });
      }
    });

  }


    @Get(':id/:imageName')
    getSnapShot(@Param() params, @Res() res: Response) {
        fs.readFile(`./tmp/${params['id']}/${params['imageName']}`,(e,data)=>{
            if(e){
                console.log('error---->', e);
                res.setHeader('Content-Type','image/png');
                res.status(404);
            } else{
                res.send(data);
            }
        })
    }


}
