import * as Http       from 'http';
import * as Https      from 'https';
import * as Qs         from 'querystring';

export interface HttpClientOptions {
  protocol: 'http:' | 'https:';
  hostname: string;
  port?: number;
  method: string;
  path: string;
  headers?: { [k: string]: string };
  body?: any;
  timeout?: number;
}

export interface Response {
  responseTime: Date;
  error: boolean;
  status: number;
  headers?: object;
  body?: any;
}

export class HttpClient {

  private requestId: number;

  public async request(opts: HttpClientOptions) {
    const requestId = ++this.requestId;
    const requestTime = new Date();
    try {
      const res = await this.doRequest(opts);
      delete opts.body;
      return res;
    } catch (e) {
      throw e;
    }
  }

  public get(path, headers?) {
    const opts: any = {
      path, headers,
      method: 'GET',
    };
    return this.request(opts);
  }

  public post(path, headers?, body?) {
    const opts: any = {
      path, headers, body,
      method: 'POST',
    };
    return this.request(opts);
  }
  public patch(path, headers?, body?) {
    const opts: any = {
      path, headers, body,
      method: 'PATCH',
    };
    return this.request(opts);
  }

  constructor() {
    this.requestId = 0;
  }

  public doRequest(opts: HttpClientOptions): Promise<Response> {
    if (!opts.headers) {
      opts.headers = {};
    }
    const transport = opts.protocol === 'http:' ? Http.request : Https.request;
    if (!opts.port) {
      opts.port = opts.protocol === 'http:' ? 80 : 443;
    }
    if (opts.body) {
      if (typeof opts.body === 'object') {
        const cType = opts.headers['Content-Type'];
        if (cType === 'application/json') {
          try {
            opts.body = JSON.stringify(opts.body);
          } catch (e) {
            console.error(e);
          }
        } else if (cType === 'application/x-www-form-urlencoded') {
          opts.body = Qs.stringify(opts.body);
        }
      }
      opts.body = Buffer.from(opts.body);
      opts.headers['Content-Length'] = opts.body.length;
    }
    return new Promise((accept, reject) => {
      const req = transport(opts, (res) => {
        let body: any = [];
        res.on('data', (chunk) => {
          body.push(chunk);
        });
        res.on('end', () => {
          body = Buffer.concat(body).toString('utf8');
          if (body) {
            const cType = res.headers['content-type'] || '';
            if (cType.indexOf('application/json') >= 0) {
              try {
                body = JSON.parse(body);
              } catch (e) {
                console.error(e);
              }
            } else if (cType.indexOf('application/x-www-form-urlencoded') >= 0) {
              body = Qs.parse(body);
            }
          }
          const responseTime = new Date();
          if (res.statusCode < 400) {
            accept({
              responseTime,
              error: false,
              status: res.statusCode,
              headers: res.headers,
              body,
            });
          } else {
            reject({
              responseTime,
              error: true,
              status: res.statusCode,
              headers: res.headers,
              body,
            });
          }
        });
      });
      req.on('error', (e) => {
        const responseTime = new Date();
        reject({
          responseTime,
          error: true,
          status: 0,
          body: {
            error: e.stack || e,
          },
        });
      });
      if (opts.body) {
        req.write(opts.body);
      }
      req.end();
    });
  }
}
