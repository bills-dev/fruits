import {Module}        from '@nestjs/common';
import {EventsGateway} from './events.gateway';
import { DeviceModule } from "../device/device.module";
import { SessionModule } from "../session/session.module";
import { MessagesModule } from "../messages/messages.module";


@Module({
  imports: [DeviceModule, SessionModule, MessagesModule],
  providers: [ EventsGateway ],
})
export class EventsModule {
}