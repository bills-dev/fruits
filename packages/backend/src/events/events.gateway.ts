import {
    SubscribeMessage,
    WebSocketGateway,
    WebSocketServer,
    WsResponse,
    OnGatewayConnection,
    OnGatewayDisconnect,
}                                from '@nestjs/websockets';
import {Inject, Injectable}      from '@nestjs/common';
import {IncomingMessage}         from 'http';
import * as WebSocket            from 'ws';
import * as Url                  from 'url';
import {ClientInfo, SessionType} from "./client";
import {DeviceService}           from "../device/device.service";
import {SessionService}          from "../session/session.service";
import {ISession, SessionEvents} from "../session/models/Session";
import {ReducerAction}           from "react";
import {IReduxAction}            from "../session/models/ReduxAction";
import { MessagesService } from "../messages/messages.service";
import * as fs from "fs";

function sendEvent(client: WebSocket, event: string, data: any) {
    client.send(JSON.stringify({
        event,
        data,
    }))
}

@WebSocketGateway()
export class EventsGateway implements OnGatewayConnection<WebSocket>, OnGatewayDisconnect<WebSocket> {

    @Inject()
    devices: DeviceService;
    @Inject()
    sessions: SessionService;
    @Inject()
    fbMessages: MessagesService;

    private clients = new Map<WebSocket, ClientInfo>();

    private getUserClients() {
        return Array.from(this.clients.entries()).filter((v) => v[1].type === 'user');
    }

    private getUserClient(sessionId) {
        let cl = this.getUserClients().find(([k, v]) => (v.session.id == sessionId));
        return cl && cl[0]
    }

    private getAdminClients() {
        return Array.from(this.clients.entries()).filter((v) => v[1].type === 'admin');
    }

    private getAdminClient(sessionId) {
        let cl = this.getAdminClients().find(([k, v]) => (v.session.id == sessionId));
        return cl && cl[0]
    }

    private getWatcherClients(sid?: string) {
        return Array.from(this.clients.entries()).filter((v) => {
            const isWatcher = v[1].type === 'watcher';
            if (sid) {
                return isWatcher && v[1].session.remoteSessionId === sid;
            }
            return isWatcher;
        });
    }

    private broadcastToUsers(event: string, data: any) {
        this.getUserClients().forEach(v => {
            sendEvent(v[0], event, data);
        })
    }

    private broadcastToAdmins(event: string, data: any) {
        this.getAdminClients().forEach(v => {
            sendEvent(v[0], event, data);
        })
    }

    private broadcastToWatchers(event: string, data: any) {
        this.getWatcherClients().forEach(v => {
            sendEvent(v[0], event, data);
        })
    }

    @SubscribeMessage('redux')
    onRedux(client: WebSocket, data: IReduxAction) {
        //console.info('redux', data);
        const info = this.clients.get(client);
        if (info) {
            const session = info.session;
            if (session) {
                session.actions.push(data);
                for (const [client, info] of this.getWatcherClients(session.id)) {
                    sendEvent(client, 'redux-action', {
                        action: data.action
                    });
                }
            }
        }
    }

    @SubscribeMessage('ping')
    onPing(client: WebSocket, data: any) {
        sendEvent(client, 'pong', data);
    }
    @SubscribeMessage('push-subscribe')
    onPushSubscribe(client: WebSocket, data: any) {
        const info = this.clients.get(client);
        if(info) {
            const device = this.devices.getDevice(info.session.deviceId);
            if (device) {
                device.pushNotifications = true;
            }
            this.broadcastToAdmins('device-update', device)
        }
    }
    @SubscribeMessage('wizard-update')
    onWizardUpdate(client: WebSocket, data: any) {
        const info = this.clients.get(client);
        if(info && info.session) {
            info.session.wizard = {
              ...info.session.wizard,
              ...data,
            };
          this.broadcastToAdmins(SessionEvents.UpdateSession, info.session);
        }
    }


    @SubscribeMessage('call-status')
    onCallStatus(client: WebSocket, { sessionId, callStatus }: {sessionId:string,callStatus:string}) {
        //client can be both
        const info = this.clients.get(client);
        if(info){
            if(info.type =='user'){
                const session = info.session;
                session.callStatus = callStatus;
                sendEvent(this.getUserClient(info.session.id), 'call-status', {callStatus});
                this.broadcastToAdmins(SessionEvents.UpdateSession, session);
            }
            if(info.type =='admin'){
                let session = this.sessions.getSession(sessionId);
                session.callStatus = callStatus;
                sendEvent(this.getUserClient(sessionId), 'call-status', {callStatus});
                this.broadcastToAdmins(SessionEvents.UpdateSession, session);
            }
        }
    }


    @SubscribeMessage('answer')
    onAnswer(client: WebSocket, { sessionId, callStatus }: { sessionId: string, callStatus: string }) {
        //client is admin
        const info = this.clients.get(client);
        let clientSession = this.sessions.getSession(sessionId);
        clientSession.callStatus = callStatus;
        this.broadcastToAdmins(SessionEvents.UpdateSession, clientSession);
        sendEvent(this.getUserClient(sessionId), 'answer', {
            callStatus, sessionId: info.session.id,
        })

    }


    @SubscribeMessage('call')
    onCall(client: WebSocket, data: any) {
        //client is flow
        const info = this.clients.get(client);
        if (info) {
            const session = info.session;
            session.callStatus = data.callStatus;
            this.broadcastToAdmins(SessionEvents.UpdateSession, session)
        }
    }


    @SubscribeMessage('sdp')
    onSdp(client: WebSocket, data: { sessionId: string, sdp: any ,ice:any}) {
        const {sessionId} = data

        const info = this.clients.get(client);
        if (info.type == 'admin') {
            console.log(`SENDING sdp ${data.sdp ? "SDP" :"ICE"} TO user `, sessionId);
            sendEvent(this.getUserClient(sessionId), 'sdp', {...data,...{from:info.session.id}})
        } else {
            console.log(`SENDING sdp ${data.sdp ? "SDP" :"ICE"} TO ADMIN `, sessionId);
            sendEvent(this.getAdminClient(sessionId), 'sdp', {...data,...{from:info.session.id}})
        }
    }

    // @SubscribeMessage('video-offer')
    // onVideoOffer(client: WebSocket, { sessionId, sdp }: { sessionId: string, sdp: string }) {
    //     //client is flow
    //     const info = this.clients.get(client);
    //     if (info.type == 'admin') {
    //         sendEvent(this.getUserClient(sessionId), 'video-offer', { sdp })
    //     } else {
    //         console.log("SENDING OFFER TO ADMIN ", sessionId)
    //         sendEvent(this.getAdminClient(sessionId), 'video-offer', { sdp })
    //     }
    // }
    //
    // @SubscribeMessage('video-answer')
    // onVideoAnswer(client: WebSocket, { sessionId, sdp }: { sessionId: string, sdp: string }) {
    //     //client is admin
    //     const info = this.clients.get(client);
    //     if (info.type == 'admin') {
    //         sendEvent(this.getUserClient(sessionId), 'video-answer', { sdp })
    //     } else {
    //
    //         sendEvent(this.getAdminClient(sessionId), 'video-answer', { sdp })
    //     }
    // }
    //
    // @SubscribeMessage('new-ice-candidate')
    // onIceCandidate(client: WebSocket, { sessionId, candidate }: { sessionId: string, candidate: any }) {
    //     const info = this.clients.get(client);
    //     if (info.type == 'admin') {
    //         sendEvent(this.getUserClient(sessionId), 'new-ice-candidate', { candidate })
    //     } else {
    //         console.log("SENDING OFFER TO ADMIN ", sessionId)
    //         sendEvent(this.getAdminClient(sessionId), 'new-ice-candidate', { candidate })
    //     }
    //     //client can be both
    // }

    handleConnection(client: WebSocket, request: IncomingMessage) {
        const url = Url.parse(request.url, true);
        const [deviceId] = url.pathname.substr(1).split('\/');
        const type = (url.query.type || 'user') as SessionType;
        const fbRef = (url.query.ref) as string;
        const key = request.headers['sec-websocket-key'] as string;
        const sid = Buffer.from(key, 'base64').toString('hex');
        console.info('Connected', type, deviceId, sid);
        let session: ISession = {
            id: sid,
            type,
            active: true,
        };
        switch (type) {
            case "user":
                const device = this.devices.addDevice({
                    id: deviceId,
                    pushNotifications: false,
                });
                session = {
                    ...session,
                    deviceId: device.id,
                    actions: [],
                };
                break;
            case "watcher":
                const remoteSessionId = url.query.sid as string;
                console.info('remoteSessionId', remoteSessionId)
                session = {
                    ...session,
                    remoteSessionId,
                };
                const remoteSession = this.sessions.getSession(remoteSessionId);
                if (remoteSession) {
                    sendEvent(client, 'welcome', {
                        active: remoteSession.active,
                        actions: remoteSession.actions,
                    });
                }
                break;
            case "admin":
                session = {
                    ...session,
                };
                sendEvent(client, 'welcome', {
                    devices: this.devices.getDevices(),
                    sessionId:sid
                });
                break;
        }
        this.devices.addSession(session);
        this.clients.set(client, {
            deviceId: deviceId,
            session,
            url,
            headers: request.headers,
            type
        });
        if (type == 'user') {
            this.broadcastToAdmins(SessionEvents.AddSession, session);
            const fbSession = this.fbMessages.getFbSession(fbRef);
            if (fbSession) {
              session.fbRefId = fbRef;
              const [firstName, lastName] = fbSession.formData.name.split(' ');
              session.wizard = {
                ...fbSession.formData,
                firstName,
                lastName
              }
              fbSession.landed = true;
                sendEvent(client, 'welcome', {
                  fbSession
                })
              this.broadcastToAdmins(SessionEvents.UpdateSession, session);
            }
        }
    }

    handleDisconnect(client: WebSocket) {
        console.info('Disconnected');
        const info = this.clients.get(client);
        this.clients.delete(client);
        if (info) {
            const session = info.session;
            if (session) {
                session.active = false;
                session.callStatus=null
            }
            if (session.type == 'user') {
                this.broadcastToAdmins(SessionEvents.UpdateSession, session);
            }

        }
    }

    @SubscribeMessage('snapshot')
    onSnapshot(client: WebSocket, data: { image: string, name: string }) {
        let stepName = String(data.name).split(' ').join('-').toLowerCase().replace('?','');
        console.info(stepName)
        const sid = this.clients.get(client).session.id;
        let directory = './tmp';
        let clientDir = `${directory}/${sid}`;
        let filename = `/${stepName}.png`;
        let img = data.image.replace(/^data:image\/\w*;base64,/, '');

        let createBase64File = function(dir,name){
            fs.writeFile(`${dir}${name}`, img, 'base64', function (err) {
                let msg = err ? err : "success: "+name;
                console.log(msg);
            });
        };

        let createFolderWithFile = function(dir, file){
            fs.mkdir(dir, {recursive: true}, function (e){
                if (e){
                    return Error(`Can't create ${dir} directory`);
                }
                return createBase64File(dir, file);
            })
        };

        fs.access(directory, fs.constants.F_OK, (err) => {
            console.log(`${directory} ${err ? 'does not exist' : 'exists'}`);
            if(err){
                fs.mkdir(directory, {recursive: true}, function (error) {
                    if (error){
                        return Error(`Can't create ${directory} directory`)
                    } else {
                        createFolderWithFile(clientDir, filename);
                    }
                })
            } else {
                fs.access(clientDir, fs.constants.F_OK, function (er){
                    if(er){
                        createFolderWithFile(clientDir, filename);
                    } else {
                        return createBase64File(clientDir, filename);
                    }
                })
            }
        });
    }
}

