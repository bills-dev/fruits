import { UrlWithParsedQuery } from "url";
import { ISession } from "../session/models/Session";

export type SessionType = 'user' | 'admin' | 'watcher';

export interface ClientInfo {
  deviceId: string;
  session: ISession;
  type: SessionType;
  url: UrlWithParsedQuery,
  headers: any,
}