import {ACTION_TYPES} from "../constants";

export const updateAdmin = (admin) => ({
    type: ACTION_TYPES.UPDATE_ADMIN,
    payload: admin
});