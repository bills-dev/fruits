import {ACTION_TYPES} from "../constants";

export const initDevices = (sessions) => ({
    type: ACTION_TYPES.INIT_DEVICES,
    payload: sessions
});
export const updateSession = (session) =>{
    return {
        type: ACTION_TYPES.UPDATE_SESSION,
        payload: session
    };
}
export const updateDevice = (device) =>{
    return {
        type: ACTION_TYPES.UPDATE_DEVICE,
        payload: device
    };
}
export const addSession = (session) =>{
    return {
        type: ACTION_TYPES.ADD_SESSION,
        payload: session
    };
}