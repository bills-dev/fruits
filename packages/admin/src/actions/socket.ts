import {ACTION_TYPES} from "../constants";
export const connect = ()=>({
    type:ACTION_TYPES.SOCKET_CONNECT
});

export const error = (message)=>({
    type:ACTION_TYPES.SOCKET_ERROR,
    payload:message
});

export const trigger = (message)=>({
    type:ACTION_TYPES.SOCKET_MESSAGE,
    payload:message
});
export const answerCell = (sessionId)=>({
    type:ACTION_TYPES.SOCKET_MESSAGE,
    payload:{sessionId}
});