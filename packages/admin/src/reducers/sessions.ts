import {ACTION_TYPES} from '../constants';
import {InitialState} from "../store/State";

export default (state: any = InitialState.sessions, action: any) => {
    const { type, payload } = action;
    switch (type) {
        case ACTION_TYPES.INIT_DEVICES: {
            let byId = {};
            payload.forEach((device) => {
                device.sessions.forEach(( { id, ...rest }) => {
                    byId[id] = { id, ...rest };
                });
            });
            let ids = Object.keys(byId);
            return { byId, ids}
        }

        case ACTION_TYPES.ADD_SESSION: {
            let session = {};
            session[payload.id] = payload;
            state.ids.push(payload.id);
            return {
                ...state,
                byId: {
                    ...state.byId,
                    ...session
                },
                ids:[
                  ...state.ids,
                ]
            };
        }
        case ACTION_TYPES.UPDATE_SESSION: {
            let session = {};
            session[payload.id] = payload;
            return {
                ...state,
                byId: {
                    ...state.byId,
                    ...session
                }
            };
        }
        default:
            return state;
    }
};
