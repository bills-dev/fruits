import {ACTION_TYPES} from '../constants';
import {InitialState} from "../store/State";

export default (state: any = InitialState.push, action: any) => {
    const { type, payload } = action;
    switch (type) {
        case ACTION_TYPES.INIT_PUSH: {
            return {
                ...state,
                payload,
            };
        }
        default:
            return state;
    }
};
