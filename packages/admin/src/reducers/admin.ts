import {ACTION_TYPES} from '../constants';
import {InitialState} from "../store/State";

export default (state: any = InitialState.admin, action: any) => {
    const { type, payload } = action;
    switch (type) {
        case ACTION_TYPES.UPDATE_ADMIN: {
            return { ...state, ...action.payload };
        }
        default:
            return state;
    }
};
