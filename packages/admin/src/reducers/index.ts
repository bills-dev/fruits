import push              from './push';
import sessions           from './sessions';
import devices           from './devices';
import admin           from './admin';
import {combineReducers} from 'redux';
import {reducer as form} from 'redux-form';

export default combineReducers({
    push,
    devices,
    sessions,
    admin,
    form,
});
