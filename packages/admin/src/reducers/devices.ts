import {ACTION_TYPES} from '../constants';
import {InitialState} from "../store/State";
import sessions       from "./sessions";

export default (state: any = InitialState.devices, action: any) => {
    const { type, payload } = action;
    switch (type) {
        case ACTION_TYPES.INIT_DEVICES: {
            let devices = payload;
            let byId = payload.reduce((normalized, { id, pushNotifications,sessions }) => {
                let sessionIds = sessions.map((session) => (session.id));
                normalized[id] = { id,pushNotifications, sessionIds };
                return normalized;
            }, {});
            let ids = Object.keys(byId);
            return { byId, ids }
        }

        case ACTION_TYPES.ADD_SESSION: {
            let device = {};

            device[payload.deviceId] = {
                id      : payload.deviceId,
                sessions: [payload.id],
            };

            if(!state.ids.includes(payload.deviceId)){
              state.ids.push(payload.deviceId);
            }

            return {
                ...state,
                byId: {
                    ...state.byId,
                },
                ids : [
                    ...state.ids,
                ],
            };
        }

        case ACTION_TYPES.UPDATE_DEVICE: {
            return {
                ...state,
                byId:{
                    ...state.byId,
                    [payload.id]: {
                      ...state.byId[payload.id],
                      ...payload,
                    }
                }
            };
        }
        default:
            return state;
    }
};
