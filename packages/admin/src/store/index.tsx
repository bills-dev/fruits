import {createSocketMiddleware} from "../middlewares/socket";
import {compose}                from 'redux';
import {createStore}            from 'redux';
import {applyMiddleware}        from 'redux';
import reducers                 from '../reducers';
import thunk                    from 'redux-thunk';

export function configureStore() {
    const socketMiddleware = createSocketMiddleware();


    const composeEnhancers = (
      (process.env.NODE_ENV !== 'production' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) ?
        window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
            name: 'FFN-ADMIN', actionsBlacklist: ['REDUX_STORAGE_SAVE']
        }) : compose
    );


    const store = createStore(
        reducers,
        composeEnhancers(applyMiddleware(thunk, socketMiddleware))
    );
    return store
}
