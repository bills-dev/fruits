export interface State {
    devices:{
        byId?: {
            [id: string]: any
        }
        ids?: Array<any>,
    }
    sessions:{
        byId?: {
            [id: string]: any
        }
        ids?: Array<any>,
    }
    admin:{
        sessionId
    },
    push:any
    form:any
}
export const InitialState: State = {
    devices: {
        byId:{},
        ids:[]
    },
    sessions: {
        byId:{},
        ids:[]
    },
    admin:{
        sessionId:null
    },
    push: {},
    form: null
};