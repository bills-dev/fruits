import {signal, Signal} from "../decorators/Signal";
import * as url         from "url";

class Socket {
    static uuid() {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            let r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    }

    static getDeviceId() {
        let deviceId = localStorage.getItem("device_id")
        if (!deviceId) {
            deviceId = Socket.uuid();
            localStorage.setItem('device_id', deviceId);
        }
        return deviceId
    }

    ws: WebSocket;

    public get state() {
        return this.ws && this.ws.readyState;
    }

    @signal
    onConnect: Signal<() => void>;

    @signal
    onError: Signal<(event) => void>;

    @signal
    onMessage: Signal<(event) => void>;

    constructor(type?: string) {

    }

    connect(){
        this.ws = new WebSocket(`${window['WS_HOST']}/?type=admin`);
        this.ws.onopen = () => this.onConnect();
        this.ws.onerror = (event) => this.onError(event);
        this.ws.onmessage = (event) => this.onMessage(event);
        this.onConnect.attach(() => {
            setInterval(() => {
                this.emit('ping', Date.now());
            }, 30000)
        })
    }


    emit(event, data) {
        this.ws.send(
            JSON.stringify({
                event, data
            }),
        );
    }

    sendReduxAction(action) {
        this.emit('redux', {
            timestamp: new Date(),
            action: action
        });
    }
}
export const socket = new Socket(window.location.search);