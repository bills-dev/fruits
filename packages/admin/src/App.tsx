import * as React                  from "react";
import CRouter                     from "./components";
import { Switch }                  from "react-router-dom";
import { BrowserRouter as Router } from "react-router-dom";
import { PushApp }                 from "./components/Push/App";
import CallApp                     from "./components/Call/App";
import {Snapshot}                  from "./components/Snapshot";
import { Messages }                from "./components/Push/Messages";

export class App extends React.Component<any, any> {
  render() {
    return (
      <Router>
        <Switch>
          <CRouter path="/" component={CallApp} exact/>
          <CRouter path="/snapshots/:sessionId" component={Snapshot}/>
          <CRouter path="/messages/:deviceId" component={Messages}/>
          <CRouter path="/push" component={PushApp}/>
        </Switch>
      </Router>
    );
  }
}
