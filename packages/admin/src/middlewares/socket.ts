import { updateDevice, updateSession } from "../actions/session";
import {addSession}                    from "../actions/session";
import {socket}                        from "../services/Socket";
import {initDevices}                   from "../actions/session";
import {ACTION_TYPES}                  from "../constants";
import {connect}                       from "../actions/socket";
import {error}                         from "../actions/socket";
import {trigger}                       from "../actions/socket";
import {updateAdmin}                   from "../actions/admin";

export function createSocketMiddleware() {
    return ({ getState, dispatch }) => next => {
        socket.connect();
        socket.onConnect.attach(() => dispatch(connect()));
        socket.onError.attach((err) => dispatch(error(err)));
        socket.onMessage.attach((event) => {
            let message = JSON.parse(event.data);
            dispatch(trigger(message));
            switch (message.event) {
                case 'welcome':
                    dispatch(initDevices(message.data.devices));
                    dispatch(updateAdmin({sessionId:message.data.sessionId}));
                    break;
                case 'update-session':
                    dispatch(updateSession(message.data));
                    break;
                case 'device-update':
                    dispatch(updateDevice(message.data));
                    break;
                case 'add-session':
                    dispatch(addSession(message.data));
                    break;
                default:
                    break
            }
        });
        return action => {
            switch (action.type) {
                case ACTION_TYPES.SOCKET_CONNECT:
                    break;
                default:
                    break;
            }


            return next(action);
        }
    }
}