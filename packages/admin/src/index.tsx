import {configureStore} from "./store";
import React            from "react";
import {App}            from "./App";
import ReactDOM         from "react-dom";
import {Provider}       from "react-redux";
import "./styles/index.scss";

async function main() {
    const store = configureStore();
    ReactDOM.render(
        <Provider store={store}>
            <App/>
        </Provider>,
        document.getElementById("root")
    );
}

window.onload = () => {
    main().catch((e) => {
        console.error("Something went wrong", e);
    });
};
