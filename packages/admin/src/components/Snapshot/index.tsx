import * as React from "react";
import {Fragment} from "react";
import {Component} from "react";
export class Snapshot extends Component<any, any> {
    state = {
        images: []
    };

    componentDidMount() {
        console.info(this.props.match.params.sessionId);
        let sid = this.props.match.params.sessionId;
        fetch(`${window['API_HOST']}/api/snapshots`, {
            method: "POST",
            headers: {"Content-Type": "application/json"},
            body: JSON.stringify({sid})
        }).then(async res => {
            let images = await res.json();
            this.setState({
                images: images
            });
            console.log('images===>>>>', images)
        })
    }

    render = () => {
        return (
            (this.state.images || []).map((name,index) => {
                return (
                    <Fragment key={index}>
                        <h2>{name.replace('.png', '').split('-').map(str => str[0].toUpperCase() + str.slice(1)).join(' ')}</h2>
                        <img src={`${window['API_HOST']}/api/snapshots/${this.props.match.params.sessionId}/${name}`} style={{border:"2px solid #333",borderRadius:"5px", width: '100%'}}/>;
                    </Fragment>
                )
            })
        )
    }
}