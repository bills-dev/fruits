import * as React             from "react";
import { Component }          from "react";
import { RefObject }          from "react";
import {socket} from "../../services/Socket";
import {connect}            from 'react-redux';
import {State} from "../../store/State";
import AudioAnalyser from "./AudioAnalyser";



const peerConnectionConfig:any = {
    'iceServers': [
        {
            'url': 'stun:stun.l.google.com:19302'
        },
        {
            'url': 'turn:192.158.29.39:3478?transport=udp',
            'credential': 'JZEOEt2V3Qb0y27GRntt2u2PAYA=',
            'username': '28224511:1379330808'
        },
        {
            'url': 'turn:192.158.29.39:3478?transport=tcp',
            'credential': 'JZEOEt2V3Qb0y27GRntt2u2PAYA=',
            'username': '28224511:1379330808'
        }
    ]
};
export interface StreamerProps {
    sessionId:string;
    adminSessionId:string;
    onCallEnd:()=>void;
}

class Streamer extends Component<StreamerProps,any>{

    private socketListener : any;
    private connection : RTCPeerConnection;
    private videoRef: RefObject<any>;
    private audioRef: RefObject<any>;
    private stream: any;

    state={
        incomingAudio:null
    };

    constructor(props){
        super(props);
        this.videoRef = React.createRef();
        this.audioRef = React.createRef();
    }

    get remoteDeviceId(){
        return this.props.sessionId
    }
    async componentDidMount(){
        console.log("Streamer componentDidMount");
        this.socketListener = socket.onMessage.attach( async (event) => {
            let message = JSON.parse(event.data);
            switch (message.event) {
                case 'update-session':
                    if(message.data.id == this.props.sessionId && !message.data.callStatus){
                        this.props.onCallEnd();
                        //this.stopPeerConnection();
                    }
                    break;
                case 'answer':
                    this.handleAnswer(message.data);
                    break;
                case 'sdp':
                    console.log("WHAITING FOR ->",this.remoteDeviceId, ' GOR ->',message.data.from);
                    this.gotSdp(message.data);
                    break;
                default:
                    break
            }
        });

        this.onAnswerClick()
    };


    stopCall = async ()=>{
        socket.emit('call-status', { callStatus:null, sessionId: this.remoteDeviceId });
    };

    onAnswerClick = () => {
        console.log('ON ANSER CLOCK;');
        socket.emit('answer', {
            callStatus: 'CONNECTING',
            sessionId: this.props.sessionId
        })
    };

    handleAnswer = ({ callStatus, sessionId: remoteDeviceId }) =>{
        // this.setState({ callStatus, remoteDeviceId })
        // if (callStatus == 'CONNECTING') {
        //     this.startCall()
        // }
    }
    remoteSet:boolean = false;
    iceQueue:any = [];
    gotSdp = async (signal) => {
        if (!this.connection) {
            await this.startPeerConnection();
        }
        if (signal.sdp) {
            console.log("sdp received",this.connection);

            await this.connection.setRemoteDescription(new RTCSessionDescription(signal.sdp));
            if (signal.sdp.type == 'offer') {
                this.remoteSet = true;
                const answer = await  this.connection.createAnswer();
                this.connection.setLocalDescription(answer);
                if(this.iceQueue.length){
                    this.iceQueue.forEach( async(s)=>{
                        console.log("SETing queued ICES ")
                        await this.connection.addIceCandidate(new RTCIceCandidate(s.ice))
                    })
                }
                console.log('SENDING sdp TO USER', this.remoteDeviceId);
                socket.emit('sdp', { sdp: this.connection.localDescription, sessionId: this.remoteDeviceId })

            }
        } else if (signal.ice) {
            try{
                console.log("ICE received",this.remoteSet);
                if(this.remoteSet){
                    await this.connection.addIceCandidate(new RTCIceCandidate(signal.ice))
                }else{
                    this.iceQueue.push(signal);
                }
            }catch (e) {
                console.error(e);
            }

        }
    };


    getAudioStream() {
        return new Promise((resolve, reject) => {
            navigator.getUserMedia({
                video: false,
                audio: true
            }, (stream) => {
                resolve(stream);
            }, function (error) {
                console.error(error);
                reject("Camera capture failed!");
            });
        })
    }

    sendIceCondidate = ({ candidate }) =>{
        console.log('SENDING ICE TO USER', this.remoteDeviceId);
        candidate && socket.emit('sdp', { ice: candidate, sessionId: this.remoteDeviceId })
    }

    gotRemoteStreamTrack = (event) => {
        const streams = event.streams[0];
        console.log('got remote streeam track',streams);
        if(streams.getTracks()[0].kind =='audio'){
            console.log('got remote audio track',streams);
            this.audioRef.current.setAttribute('autoplay', 'true');
            this.audioRef.current.srcObject = event.streams[0];
        }else{
            console.log('got remote Video track',streams);
            this.videoRef.current.setAttribute('autoplay', 'true');
            this.videoRef.current.srcObject = event.streams[0];
        }
        this.setState({incomingAudio:event.streams[0]});
        socket.emit('call-status', { callStatus:'IN_CALL', sessionId: this.remoteDeviceId })
    };

    gotRemoteStream = (event) => {
        console.log('got remote streeam');
        this.videoRef.current.setAttribute('autoplay', 'true');
        this.videoRef.current.setAttribute('autoplay', 'true');
        this.videoRef.current.srcObject = event.streams
       // this.setState({incomingAudio:event.streams})

    }


    oniceconnectionstatechange = (event) => {
        if(this.connection){
            switch(this.connection.iceConnectionState) {
                case "closed":
                case "failed":
                case "disconnected":
                    this.stopPeerConnection();
                    this.props.onCallEnd();
                    //this.stopCall();
                    break;
            }
        }

    };

    onsignalingstatechange = (event) => {
        if(this.connection){
            switch(this.connection.signalingState) {
                case "closed":
                    this.stopPeerConnection();
                    this.props.onCallEnd();
                    //this.stopCall();
                    break;
            }
        }

    };

    async startPeerConnection() {
        console.log('startPeerConnection', this.remoteDeviceId);
        const peerConnection: any = this.connection = new RTCPeerConnection(peerConnectionConfig);
        peerConnection.oniceconnectionstatechange = this.oniceconnectionstatechange;
        peerConnection.onsignalingstatechange = this.onsignalingstatechange;
        const stream = this.stream = await this.getAudioStream();
        peerConnection.onicecandidate = this.sendIceCondidate;
        peerConnection.ontrack = this.gotRemoteStreamTrack;
        peerConnection.onstream = this.gotRemoteStream;
        await peerConnection.addStream(stream);
        return
    }

    async stopPeerConnection() {
        console.log('stopPeerConnection', this.remoteDeviceId);
        const peerConnection: any = this.connection;
        if(peerConnection){
            peerConnection.onicecandidate = null;
            peerConnection.ontrack = null;
            peerConnection.onstream = null;
            if(this.videoRef && this.videoRef.current){
                this.videoRef.current.srcObject =null;
            }
            peerConnection.close();
        }
        if(this.stream){
            this.stream.getTracks()[0].stop();
        }
        this.connection = null;
        return
    }



    componentWillUnmount(){
        console.log("Streamer componentWillUnmount")
        socket.onMessage.detach(this.socketListener);
        this.stopPeerConnection();
        //socket.emit('call-status', { callStatus:null, sessionId: this.remoteDeviceId })
    }

    render(){

        return (<div>
            <h1>{this.props.sessionId}</h1>
            {this.state.incomingAudio ? <AudioAnalyser audio={this.state.incomingAudio} /> : ''}
            <video style={{width:"100%"}} controls={true} ref={this.videoRef}/>
            <audio style={{width:"100%"}} controls={true} ref={this.audioRef}/>
        </div>)
    }
}
const mapStateToProps = (state: State) => ({
    adminSessionId:state.admin.sessionId
});
const mapDispatchToProps = (dispatch) => {};
export default (connect(mapStateToProps)(Streamer));