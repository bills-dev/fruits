import * as React           from "react";
import {Component}          from "react";
import {RefObject}          from "react";
import {bindActionCreators} from 'redux'
import {connect}            from 'react-redux';
import {State}              from "../../store/State";
import {Alignment, Label}   from "@blueprintjs/core";
import {Switch}             from "@blueprintjs/core";
import {InputGroup}         from "@blueprintjs/core";
import {NavbarGroup}        from "@blueprintjs/core";
import {Button}             from "@blueprintjs/core";
import {AnchorButton}       from "@blueprintjs/core";
import {MenuDivider}        from "@blueprintjs/core";
import {Navbar}             from "@blueprintjs/core";
import {Position}           from "@blueprintjs/core";
import {Dialog}             from "@blueprintjs/core";
import {Tag}                from "@blueprintjs/core";
import {MenuItem}           from "@blueprintjs/core";
import {Popover}            from "@blueprintjs/core";
import {Menu}               from "@blueprintjs/core";
import {Card}               from "@blueprintjs/core";
import {ButtonGroup}        from "@blueprintjs/core";
import {answerCell}         from "../../actions/socket";
import Streamer             from "./Streamer";
import {Create}             from "../Push/Messages/create";


interface CallProps {
  devices: any
  sessions: any
  deviceIds: string[]
}

const steps = {
  1: 'Loan Details',
  2: 'PI',
  3: 'Employment',
  4: 'Address',
  5: 'Birthday',
  6: 'Thank you',
};

class Call extends Component<CallProps, any> {
  state = {
    filterActive: false,
    dialogOpen: null,
    activeDeviceId: "",
    isOpenNfDialog: false,
  };

  private filterActiveSessions(sessions, deviceId) {
    return Object.values(sessions).filter((session: any) => {
      if (session.deviceId === deviceId) {
        if (this.state.filterActive) {
          return session.active;
        }
        return session;
      }
    });
  }
  private getSessionName(deviceId) {
    const sessions: any[] = Object.values(this.props.sessions).filter((s: any) => s.deviceId === deviceId).reverse();
    for (const s of sessions) {
      const formData = s.wizard && s.wizard.formData;
      if (formData) {
        if (formData.firstName) {
          return `${formData.firstName} ${formData.lastName || ''}`
        }
      }
    }
    return 'Unknown Visitor';
  }

  initiateCall = (sessionId) => {
    this.setState({ dialogOpen: sessionId })
  };

  closeDialog = () => {
    this.setState({ dialogOpen: null })
  };
  onSendMessageClick = (e, deviceId) => {
    e.preventDefault();
    this.setState({
      isOpenNfDialog: true,
      activeDeviceId: deviceId,
    })
  };


  render() {
    const {
      devices,
      deviceIds,
    } = this.props;

    return (
      <Card elevation={3} style={{ marginTop: 20 }}>
        <Switch
          label="Show Active"
          onChange={(value: any) => {
            this.setState({
              filterActive: value.target.checked
            })
          }}
        />
        {deviceIds.length ? this.renderDevices(deviceIds, devices) : this.renderNoDevices()}
        <Dialog
          canOutsideClickClose={false}
          style={{ minWidth: 1366, padding: 0 }}
          icon="info-sign"
          title="New Message"
          isOpen={this.state.isOpenNfDialog}
          onClose={() => {
            this.setState({
              isOpenNfDialog: false,
              activeDeviceId: "",
            })
          }}
        >
          <Create deviceId={this.state.activeDeviceId}/>
        </Dialog>
        <Dialog
          icon="info-sign"
          onClose={this.closeDialog}
          title="Palantir Foundry"
          usePortal={true}
          style={{ width: 900 }}
          isOpen={this.state.dialogOpen}
        >
          <Streamer sessionId={this.state.dialogOpen} onCallEnd={this.closeDialog}/>
        </Dialog>
      </Card>
    );
  }
  renderNoDevices() {
    return <div>No devices</div>
  }
  renderDevices(deviceIds, devices) {
    return <table className="bp3-html-table bp3-html-table-bordered">
      <thead>
      <tr>
        <th>#</th>
        <th style={{width:'100%'}}>Name</th>
        <th style={{ textAlign: "center", minWidth:'110px' }}>Loan Amount</th>
        <th style={{ textAlign: "center", minWidth:'200px' }}>Loan Purpose</th>
        <th style={{ textAlign: "center", minWidth:'50px' }}>Source</th>
        <th style={{ textAlign: "center", minWidth:'100px' }}>Current Step</th>
        <th style={{ textAlign: "right" }}>Actions</th>
      </tr>
      </thead>
      <tbody>
      {deviceIds.map(deviceId => this.renderDevice(deviceId, devices))}
      </tbody>

    </table>
  }
  renderDevice(deviceId, devices) {
    const canPushNotification = devices[ deviceId ] && devices[ deviceId ][ 'pushNotifications' ];
    const filteredSessions: any = this.filterActiveSessions(this.props.sessions, deviceId);
    return !!filteredSessions.length &&  <React.Fragment key={deviceId}>
      <tr key={deviceId} className="device-header">
        <td colSpan={8}>
          {this.renderDeviceInfo(deviceId, canPushNotification)}
        </td>
      </tr>
      {filteredSessions.map((session: any, index: number) => {
        return this.renderSession(session, deviceId, index);
      })}
    </React.Fragment>
  }
  renderDeviceInfo(deviceId, canPushNotification) {
    const renderSendMessage = () => {
      return <ButtonGroup>
        <Button icon="chat" onClick={e => this.onSendMessageClick(e, deviceId)}/>
        <AnchorButton icon="list" href={`/messages/${deviceId}`} target="_blank"/>
      </ButtonGroup>
    };
    return <div key={deviceId} style={{ display: 'flex' }}>
      <h3>{this.getSessionName(deviceId)}</h3>
      {canPushNotification && renderSendMessage()}
    </div>
  }
  renderSession(session, deviceId, index: number) {
    if (session.deviceId === deviceId) {
      const sessionId = session.id;
      const formData = session.wizard && session.wizard.formData;
      const currentStep = session.wizard && session.wizard.currentStep;
      let backgroundColor;
      if (session.active) {
        backgroundColor = '#ffffd8'
      } else {
        backgroundColor = session.wizard && session.wizard.currentStep === 6 && '#e8f7e8' || '#fff3f3'
      }
      return (
        <tr key={sessionId} style={{ backgroundColor }}>
          <td style={{ textAlign: 'center' }}>{++index}</td>
          <td>{formData && `${formData.firstName || ''} ${formData.lastName || ''}` || ''}</td>
          <td>{formData && formData.debt || ''}</td>
          <td>{formData && formData.purpose || ''}</td>
          <td style={{ textAlign: "center" }}>{session.fbRefId &&
          <img alt={"FB"} src="https://img.icons8.com/color/48/000000/facebook-messenger.png" width={24}/>}</td>


          <td>{currentStep && steps[ currentStep ] || ''}</td>
          <td style={{ textAlign: 'right' }}>
            <ButtonGroup>
              {session.callStatus && <Button icon="phone"
                                             intent="success"
                                             loading={session.callStatus === 'CONNECTING'}
                                             disabled={session.callStatus === 'IN_CALL'}
                                             onClick={() => {
                                               this.initiateCall(sessionId);
                                             }}
              />}
              <AnchorButton icon="eye-open" target="_blank"
                            href={`${window[ 'UI_HOST' ]}?type=watcher&sid=${sessionId}`}/>
              <AnchorButton icon="media" target="_blank" href={`/snapshots/${sessionId}`}/>
            </ButtonGroup>
          </td>
        </tr>
      )
    }
  }

}

const mapStateToProps = (state: State) => {
  return ({
    deviceIds: state.devices.ids,
    devices: state.devices.byId,
    sessions: state.sessions.byId,
  })
};

const mapDispatchToProps = (dispatch) => (
  bindActionCreators({ answerCell }, dispatch)
);

export default connect(mapStateToProps, mapDispatchToProps)(Call);