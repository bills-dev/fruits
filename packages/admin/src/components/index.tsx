import * as React        from 'react';
import { Link }          from 'react-router-dom';
import { Route }         from 'react-router-dom';
import { Icon }          from "@blueprintjs/core";
import { Button }        from "@blueprintjs/core";
import { Navbar }        from "@blueprintjs/core";
import { Classes }       from "@blueprintjs/core";
import { Alignment }     from "@blueprintjs/core";
import { NavbarGroup }   from "@blueprintjs/core";
import { NavbarHeading } from "@blueprintjs/core";
import { NavbarDivider } from "@blueprintjs/core";

class CRouter extends React.Component<any, any> {
  renderRoute = (props: any) => {
    const {
      component: Component,
      ...rest
    } = this.props;

    return (
      <div>
        <Navbar>
          <NavbarGroup align={Alignment.LEFT}>
            <NavbarHeading>
              <Link to='/'>
                <Icon icon="snowflake"/>
              </Link>
            </NavbarHeading>
            <NavbarDivider/>
            <Link to={`/`} style={{ marginRight: 5, marginLeft: 5 }}>
              <Button
                icon="desktop"
                text="Devices"
                className={Classes.MINIMAL}
                active={this.props.location.pathname.includes('subscribers') || this.props.location.pathname === '/'}
              />
            </Link>
          </NavbarGroup>
        </Navbar>
        <Component {...rest} {...props}/>
      </div>
    );
  };

  public render() {
    const {
      component,
      ...rest
    } = this.props;

    return <Route {...rest} render={this.renderRoute}/>;
  }
}

export default CRouter;
