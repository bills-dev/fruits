import React, { Component } from "react";
import { Icon, Position }   from "@blueprintjs/core";
import { Tag }              from "@blueprintjs/core";
import { Toaster }          from "@blueprintjs/core";
import { Tab }              from "@blueprintjs/core";
import { Tabs }             from "@blueprintjs/core";
import { Intent }           from "@blueprintjs/core";
import { Divider }          from "@blueprintjs/core";
import { Button }           from "@blueprintjs/core";
import { InputGroup }       from "@blueprintjs/core";
import { ButtonGroup }      from "@blueprintjs/core";
import { Card }             from "@blueprintjs/core";
import { FormGroup }        from "@blueprintjs/core";
import { NumericInput }     from "@blueprintjs/core";
import { EditableText }     from "@blueprintjs/core";
import { Collapse }         from "@blueprintjs/core";
import macos                from "../../../images/macos.svg";
import android              from "../../../images/android.svg";
import windows              from "../../../images/windows.svg";
import arrowRight           from "../../../images/arrowRight.svg";

export const AppToaster = Toaster.create({
  className: "recipe-toaster",
  position : Position.TOP_RIGHT,
});

export class Create extends Component<any, any> {
  public state = {
    confirmButtonLoading: false,
    selectedTab         : "macos",
    showArrowIcon       : false,
    isOpen              : true,
    form                : {
      title    : 'Great news !!!',
      message  : 'Your loan is approved, click see details ...',
      duration : 259200,
      delivery : 'now',
      iconUrl  : 'https://admin.fruits.ffn.li/logo.png',
      imageUrl : 'https://admin.fruits.ffn.li/logo.jpg',
      launchUrl: 'https://save.freedomdebtrelief.com',
    }
  };

  private static renderDuration(seconds: number) {
    if (seconds <= 0) {
      return 'now'
    }
    let days = Math.floor(seconds / (3600 * 24));
    seconds -= days * 3600 * 24;
    let hrs = Math.floor(seconds / 3600);
    seconds -= hrs * 3600;
    let mnts = Math.floor(seconds / 60);
    seconds -= mnts * 60;

    let str = {} as any;
    if (days !== 0) {
      if (days > 1) {
        str['days'] = `${days} days`;
      } else {
        str['days'] = `${days} day`;
      }
    }

    if (hrs !== 0) {
      if (hrs > 1) {
        str['hours'] = `${hrs} hours`;
      } else {
        str['hours'] = `${hrs} hour`;
      }
    }

    if (mnts !== 0) {
      if (mnts > 1) {
        str['minutes'] = `${mnts} minutes`;
      } else {
        str['minutes'] = `${mnts} minute`;
      }
    }

    if (seconds !== 0) {
      if (seconds > 1) {
        str['seconds'] = `${seconds} seconds`;
      } else {
        str['seconds'] = `${seconds} second`;
      }
    }

    return Object.values(str).join(', ').trim();
  }

  private renderWaitUntilComponent = () => {
    return (
      <div>
        <Divider style={{ width: '100%', marginLeft: 0, marginRight: 0, marginTop: 10 }}/>
        <FormGroup
          label="Wait Until"
        >
          <InputGroup
            large={true}
            onChange={(e: any) => {
              this.setState({
                form: {
                  ...this.state.form,
                  delivery: e.target.value,
                }
              })
            }}
            value={this.state.form.delivery}
            placeholder="Now"
          />
          <ButtonGroup minimal={true} style={{ minWidth: 120, marginTop: 15 }}>
            <Button disabled={true} text="For example:"/>
            <Button text={"+2 minutes"} onClick={(e: any) => {
              e.preventDefault();
              this.setState({
                form: {
                  ...this.state.form,
                  delivery: "+2 minutes",
                }
              })
            }}/>
            <Button text={"+2 hours"} onClick={(e: any) => {
              e.preventDefault();
              this.setState({
                form: {
                  ...this.state.form,
                  delivery: "+2 hours"
                }
              })
            }}/>
            <Button text={"+2 days"} onClick={(e: any) => {
              e.preventDefault();
              this.setState({
                form: {
                  ...this.state.form,
                  delivery: "+2 days"
                }
              })
            }}/>
          </ButtonGroup>
        </FormGroup>
      </div>
    )
  };

  private handleTabChange = (selectedTab: any) => this.setState({ selectedTab });

  private handleClick = () => {
    this.setState({
      isOpen: !this.state.isOpen
    })
  };
  get domainPreview(){
    try{
      return new URL(this.state.form.launchUrl).hostname
    }catch (e) {
      return 'example.com'
    }

  }
  private renderPreview(img: any, type: string = 'macos') {
    switch (type) {
      case "android": {
        return (
          <div style={{ width: "100%", textAlign: "center" }}>
            <div style={{ display: 'table', position: "relative", margin: "0 auto" }}>
              <img alt="Android" src={img}/>
              <Card elevation={4} style={{
                position: "absolute",
                top     : 290,
                width   : 405,
                padding : "5px 30px",
                right   : 0,
                left    : 0,
                margin  : "0 auto"
              }}>
                <div style={{ paddingTop: 10 }}>
                  <div onClick={this.handleClick} style={{ display: "flex", alignItems: "center" }}>
                    <svg style={{ marginRight: 5 }} width={15} className="sc-jKJlTe jyxZzl" viewBox="0 0 1024 1024">
                      <path stroke="inherit"
                            d="M258.278 446.542l-146.532-253.802c93.818-117.464 238.234-192.74 400.254-192.74 187.432 0 351.31 100.736 440.532 251h-417.77c-7.504-0.65-15.092-1-22.762-1-121.874 0-224.578 83.644-253.722 196.542zM695.306 325h293.46c22.74 57.93 35.234 121.004 35.234 187 0 280.826-226.1 508.804-506.186 511.926l209.394-362.678c29.48-42.378 46.792-93.826 46.792-149.248 0-73.17-30.164-139.42-78.694-187zM326 512c0-102.56 83.44-186 186-186s186 83.44 186 186c0 102.56-83.44 186-186 186s-186-83.44-186-186zM582.182 764.442l-146.578 253.878c-246.532-36.884-435.604-249.516-435.604-506.32 0-91.218 23.884-176.846 65.696-251.024l209.030 362.054c41.868 89.112 132.476 150.97 237.274 150.97 24.3 0 47.836-3.34 70.182-9.558z"></path>
                    </svg>
                    <span style={{ lineHeight: 1, marginRight: 3 }}>{this.domainPreview}</span>
                    {this.state.form.imageUrl && (
                      this.state.isOpen ? (<Icon icon='chevron-up'/>) : (<Icon icon='chevron-down'/>)
                    )}
                  </div>
                  <div style={{ display: "flex", justifyContent: "space-between", alignItems: "center" }}>
                    <div style={{ paddingTop: 5 }}>
                      <p style={{ color: "rgb(83, 83, 83)", textAlign: "left", marginTop: 5, marginBottom: 0 }}>
                        {this.state.form.title ? this.state.form.title : 'Title'}
                      </p>
                      <p style={{ color: "rgb(117, 117, 117)", textAlign: "left" }}>
                        {this.state.form.message ? this.state.form.message : 'Message Body'}
                      </p>
                    </div>
                    {!this.state.isOpen && <div>
                      {this.state.form.iconUrl && <img width={38} alt={"alt"} src={this.state.form.iconUrl}/>}
                    </div>}
                  </div>
                </div>
                {this.state.form.imageUrl && (
                  <Collapse isOpen={this.state.isOpen}>
                    <img style={{ marginTop: 0 }} width='100%' alt={"alt"} src={this.state.form.imageUrl}/>
                  </Collapse>
                )}
              </Card>
            </div>
          </div>
        )
      }
      case "macos": {
        return (
          <div style={{ display: "table", margin: "0 auto", textAlign: "center" }}>
            <div style={{ position: 'relative' }}>
              <img alt="Android" src={img}/>
              <Card elevation={4} style={{
                width         : 520,
                borderRadius  : 8,
                display       : 'flex',
                justifyContent: "space-between",
                position      : "absolute",
                top           : 50,
                left          : 0,
                right         : 0,
                margin        : "0 auto",
              }}>
                <div style={{ display: "flex" }}>
                  <div>
                    <svg style={{ width: 55 }} viewBox="0 0 48 48">
                      <circle cx="24" cy="24" r="19" fill="#4CAF50"></circle>
                      <path fill="#FFC107" d="M24 5v19l8 4-8.8 15h.8c10.5 0 19-8.5 19-19S34.5 5 24 5z"></path>
                      <path fill="#F44336" d="M16 28l8-13h16.7C37.5 9 31.2 5 24 5c-6.7 0-12.6 3.5-16 8.8L16 28z"></path>
                      <path fill="#DD2C00" d="M8 13.8L16 28l5-1"></path>
                      <path fill="#F9A825" d="M40.6 15H24l-1.5 4.8"></path>
                      <path fill="#558B2F" d="M23.2 43L32 28l-3.8-3.1"></path>
                      <circle cx="24" cy="24" r="9" fill="#FFF"></circle>
                      <circle cx="24" cy="24" r="7" fill="#2196F3"></circle>
                    </svg>
                  </div>
                  <div style={{ marginLeft: 5 }}>
                    <p style={{ margin: 0, textAlign: 'left' }}>
                      <strong>{this.state.form.title ? this.state.form.title : "Title"}</strong></p>
                    <p style={{ margin: 0, textAlign: 'left' }}><strong>{this.domainPreview}</strong></p>
                    <p style={{
                      margin   : 0,
                      textAlign: 'left'
                    }}>{this.state.form.message ? this.state.form.message : "Message Body"}</p>
                  </div>
                </div>
                <div>
                  {this.state.form.iconUrl && <img alt={"alt"} width={75} src={this.state.form.iconUrl}/>}
                </div>
              </Card>
            </div>
          </div>
        )
      }
      case "windows": {
        return (
          <div style={{ width: "100%", textAlign: "center" }}>
            <div style={{ position: "relative", display: "table", margin: "0 auto" }}>
              <img alt="Android" src={img}/>

              <div
                onMouseEnter={() => {
                  this.setState({
                    showArrowIcon: true,
                  })
                }}
                onMouseLeave={() => {
                  this.setState({
                    showArrowIcon: false,
                  })
                }}
                style={{
                  display        : "table",
                  minWidth       : 348,
                  backgroundColor: "#000",
                  position       : "absolute",
                  right          : 20,
                  bottom         : 60
                }}>
                <div>
                  {this.state.form.imageUrl &&
                  <img alt={"alt"} style={{ height: 230 }} src={this.state.form.imageUrl}/>}
                </div>
                <div style={{ display: 'flex', marginTop: 15, marginLeft: 15, marginRight: 15, position: "relative" }}>
                  <div>
                    {this.state.form.iconUrl &&
                    <img alt={"alt"} style={{ width: 55 }} src={this.state.form.iconUrl}/>}
                  </div>
                  <div style={{ textAlign: "left", marginLeft: 10, marginBottom: 25 }}>
                    <p style={{ margin: 0, color: '#fff' }}>
                      <strong>{this.state.form.title ? this.state.form.title : "Title"}</strong></p>
                    <p style={{ marginBottom: 10, color: 'rgb(167, 167, 167' }}>
                      {this.state.form.message ? this.state.form.message : "Message Body"}
                    </p>
                    <p style={{ margin: 0, color: 'rgb(167, 167, 167)' }}>{this.domainPreview}</p>
                    {this.state.showArrowIcon &&
                    <img style={{ position: "absolute", top: 0, right: 0 }} alt="alt" width={20} src={arrowRight}/>}
                  </div>
                </div>
              </div>
            </div>
          </div>
        )
      }
    }
  }

  public render() {
    const { deviceId } = this.props;

    return (
      <div style={{ display: 'flex' }}>
        <div style={{ padding: 5 }}>
          <Card elevation={2} className='form-sections'>
            <h3 className='bp3-heading'><Tag round={true}>1</Tag> <strong>Message</strong></h3>
            <FormGroup>
              <EditableText
                maxLines={1}
                maxLength={30}
                placeholder="Title"
                className='form-input'
                selectAllOnFocus={true}
                value={this.state.form.title}
                onChange={title => {
                  this.setState({
                    form: {
                      ...this.state.form,
                      title
                    }
                  })
                }}
              />
            </FormGroup>
            <FormGroup>
              <EditableText
                minLines={3}
                maxLines={12}
                maxLength={50}
                multiline={true}
                placeholder="Message"
                selectAllOnFocus={true}
                confirmOnEnterKey={true}
                className='form-input h80'
                value={this.state.form.message}
                onChange={message => {
                  this.setState({
                    form: {
                      ...this.state.form,
                      message
                    }
                  })
                }}
              />
            </FormGroup>
          </Card>
          <br/>
          <Card elevation={2} className='form-sections'>
            <h3 className='bp3-heading'><Tag round={true}>2</Tag> <strong>Options</strong></h3>
            <FormGroup>
              <EditableText
                maxLines={1}
                placeholder="Icon"
                className='form-input'
                selectAllOnFocus={true}
                value={this.state.form.iconUrl}
                onChange={iconUrl => {
                  this.setState({
                    form: {
                      ...this.state.form,
                      iconUrl
                    }
                  })
                }}
              />
            </FormGroup>
            <FormGroup>
              <EditableText
                maxLines={1}
                placeholder="Image"
                className='form-input'
                selectAllOnFocus={true}
                value={this.state.form.imageUrl}
                onChange={imageUrl => {
                  this.setState({
                    form: {
                      ...this.state.form,
                      imageUrl
                    }
                  })
                }}
              />
            </FormGroup>
            <FormGroup>
              <EditableText
                maxLines={1}
                className='form-input'
                selectAllOnFocus={true}
                placeholder="Launch URL"
                value={this.state.form.launchUrl}
                onChange={launchUrl => {
                  this.setState({
                    form: {
                      ...this.state.form,
                      launchUrl
                    }
                  })
                }}
              />
            </FormGroup>
            <FormGroup helperText={<p>Duration: (<span>{Create.renderDuration(this.state.form.duration)}</span>)</p>}>
              <NumericInput
                min={1}
                fill={true}
                stepSize={60}
                majorStepSize={60}
                value={this.state.form.duration}
                allowNumericCharactersOnly={true}
                onValueChange={(value) => {
                  this.setState({
                    form: {
                      ...this.state.form,
                      duration: value
                    }
                  })
                }}
              />
            </FormGroup>
          </Card>

          {/*
          <br/>
          <Card elevation={2} className='form-sections'>
            <h3 className='bp3-heading'><Tag round={true}>3</Tag> <strong>Schedule</strong></h3>
            <FormGroup>
              <RadioGroup
                inline={true}
                name="delivery"
                label="Delivery"
                selectedValue={this.state.form.delivery}
                onChange={(e: any) => {
                  this.setState({
                    form: {
                      ...this.state.form,
                      delivery: e.target.value,
                    }
                  })
                }}
              >
                <Radio label="Begin sending immediately" value="now"/>
                <Radio
                  label="Begin sending at a particular time"
                  value={this.state.form.delivery !== 'now' ? this.state.form.delivery : '+5 minutes'}
                />
                {this.state.form.delivery !== 'now' && this.renderWaitUntilComponent()}
              </RadioGroup>
            </FormGroup>
          </Card>
          */}

          <br/>
          <Card elevation={2} className='form-sections' style={{ textAlign: "right" }}>
            <Button
              loading={this.state.confirmButtonLoading}
              intent={Intent.PRIMARY}
              onClick={() => {
                if (this.state.form.title === "") {
                  AppToaster.show({
                    message: "Title is required",
                    intent : "danger",
                    icon   : "warning-sign"
                  });
                  return false;
                }
                if (this.state.form.launchUrl === "") {
                  AppToaster.show({
                    message: "Launch Url is required",
                    intent : "danger",
                    icon   : "warning-sign"
                  });
                  return false;
                }
                this.setState({
                  confirmButtonLoading: true,
                });
                fetch(`${window['API_HOST']}/api/notification/send`, {
                  method : "POST",
                  headers: {
                    'Content-type': 'application/json'
                  },
                  body   : JSON.stringify({
                    ...this.state.form,
                    deviceId,
                  })
                })
                  .then(response => response.json())
                  .then(responseJson => {
                    console.info(responseJson);
                    if (responseJson.error) {
                      AppToaster.show({
                        message: responseJson.message,
                        intent : "danger",
                        icon   : "warning-sign"
                      });
                    } else {
                      AppToaster.show({
                        message: responseJson.message,
                        intent : "success",
                        icon   : "warning-sign"
                      });
                    }
                    this.setState({
                      confirmButtonLoading: false,
                    });
                  })
                  .catch(e => {
                    console.error(e);
                  })
              }}
            >Send</Button>
          </Card>
        </div>
        <div style={{ padding: 5, flexGrow: 1 }}>
          <Tabs
            id="previewTabs"
            onChange={this.handleTabChange}
            renderActiveTabPanelOnly={true}
            selectedTabId={this.state.selectedTab}
          >
            <Tab
              id="macos"
              title="WEB: MACOS"
              className="previewTab"
              panel={this.renderPreview(macos, 'macos')}
            />
            <Tab
              id="windows"
              title="WEB: WINDOWS"
              className="previewTab"
              panel={this.renderPreview(windows, 'windows')}
            />
            <Tab
              id="android"
              title="WEB: ANDROID"
              className="previewTab"
              panel={this.renderPreview(android, 'android')}
            />
          </Tabs>
        </div>
      </div>
    );
  }
}