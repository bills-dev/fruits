import React, { Component } from "react";
import { Card }             from "@blueprintjs/core";

export class Messages extends Component<any, any> {
  public state = {
    messages: []
  };

  componentDidMount(): void {
    const { deviceId } = this.props.match.params;
    fetch(`${window['API_HOST']}/api/notification/get-messages`, {
        method : "POST",
        headers: {
          'Content-Type': 'application/json'
        },
        body   : JSON.stringify({ deviceId })
      }
    ).then(response => response.json())
      .then(responseJson => {
        if (responseJson && responseJson.data) {
          this.setState({
            messages: [
              ...this.state.messages,
              ...responseJson.data,
            ]
          })
        }
      })

  }

  public render() {
    return (
      <Card style={{ marginTop: 50 }}>
        <table className="bp3-html-table bp3-html-table-bordered">
          <thead>
          <tr>
            <th style={{ boxShadow: "none", width: 50, textAlign: 'center' }}>#</th>
            <th>Title</th>
            <th>Message</th>
            <th>Icon</th>
            <th>Image</th>
          </tr>
          </thead>
          <tbody>
          {this.state.messages.map((item, index) => {
            return (
              <tr key={index}>
                <td>{++index}</td>
                <td>{item.title}</td>
                <td>{item.body}</td>
                <td>{item.icon && <img width={50} alt={"Image"} src={item.icon}/>}</td>
                <td>{item.image && <img width={50} alt={"Image"} src={item.image}/>}</td>
              </tr>
            )
          })}
          </tbody>
        </table>
      </Card>
    );
  }
}