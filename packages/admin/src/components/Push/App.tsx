import * as React        from "react";
import { Messages }      from "./Messages";
import { Subscribers }   from "./Subscribers";
import { Route, Switch } from "react-router-dom";

export class PushApp extends React.Component<any, any> {
  render() {
    const {
      match
    } = this.props;

    console.info(match);

    return (
      <Switch>
        <Route path={`${match.url}/messages`}    component={Messages}/>
        <Route path={`${match.url}/subscribers`} component={Subscribers}/>
      </Switch>
    );
  }
}
