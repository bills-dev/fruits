import * as React     from 'react';
import { Route }      from 'react-router-dom';
import { withRouter } from 'react-router-dom';

class PushRouter extends React.Component<any, any> {
  renderRoute = () => {
    const {
      component: Component,
      ...rest
    } = this.props;

    return (<Component {...rest}/>);
  };

  public render() {
    return <Route render={this.renderRoute}/>;
  }
}

export default withRouter(PushRouter);
