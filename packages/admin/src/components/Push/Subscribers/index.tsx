import * as faker                                                   from "faker"
import React, { Component }                                         from "react";
import { AnchorButton, Classes, Dialog, Intent, Position, Tooltip } from "@blueprintjs/core";
import { Menu }                                                     from "@blueprintjs/core";
import { MenuItem }                                                 from "@blueprintjs/core";
import { Popover }                                                  from "@blueprintjs/core";
import { MenuDivider }                                              from "@blueprintjs/core";
import { Button }                                                   from "@blueprintjs/core";
import { ButtonGroup }                                              from "@blueprintjs/core";
import { Cell }                                                     from "@blueprintjs/table";
import { Table }                                                    from "@blueprintjs/table";
import { Column }                                                   from "@blueprintjs/table";
import { RenderMode }                                               from "@blueprintjs/table";
import { SelectionModes }                                           from "@blueprintjs/table";
import { ColumnHeaderCell }                                         from "@blueprintjs/table";
import { TableLoadingOption }                                       from "@blueprintjs/table";
import { Create }                                                   from "../Messages/create";

export class Subscribers extends Component {
  public state = {
    subscribers  : [],
    activeSession: null,
    isOpen       : false,
    columns      : [
      {
        column: "device_id",
        label : "Device Id",
      },
      {
        column: "last_name",
        label : "Last Name",
      },
      {
        column: "first_name",
        label : "First Name",
      },
      {
        column: "actions",
        label : "Actions",
      },
    ],
    loading      : [TableLoadingOption.CELLS],
  };

  public componentDidMount(): void {
    fetch('http://127.0.0.1:3002/api/notification/subscribers')
      .then(res => res.json())
      .then(resJson => {
        this.setState({
          subscribers: [
            ...resJson
          ],
          loading    : [],
        })
      });
  }

  render() {
    const {
      columns,
      subscribers
    } = this.state;

    const renderColumns = (columns || []).map((col, index) => {
      return (
        <Column
          key={index}
          cellRenderer={(rowIndex: number, columnIndex: number) => {
            if (columns[columnIndex]['column'] === 'actions') {
              return (
                <Cell style={{ padding: 0 }}>
                  <>
                    <ButtonGroup
                      fill={true}
                      large={true}
                    >
                      <Popover
                        content={
                          <Menu>
                            <MenuDivider/>
                            <MenuItem
                              text="New Message"
                              icon="document-open"
                              onClick={() => {
                                this.setState({
                                  isOpen       : true,
                                  activeSession: subscribers[rowIndex]["device_id"],
                                });
                              }}
                            />
                            <MenuDivider/>
                          </Menu>
                        }
                        position={Position.BOTTOM_LEFT}
                      >
                        <Button
                          text="Actions"
                          icon="document"
                          rightIcon="caret-down"
                          style={{ borderRadius: 0, border: "none" }}
                        />
                      </Popover>
                    </ButtonGroup>
                  </>
                </Cell>
              )
            }

            return (
              <Cell>{subscribers[rowIndex][columns[columnIndex]['column']]}</Cell>
            )
          }}
          columnHeaderCellRenderer={(columnIndex: number) => {
            return (
              <ColumnHeaderCell name={columns[columnIndex]['label']}/>
            )
          }}
        />
      )
    });

    return (
      <>
        <Table
          defaultRowHeight={40}
          className="tb-subscribers"
          renderMode={RenderMode.NONE}
          loadingOptions={this.state.loading}
          selectionModes={SelectionModes.NONE}
          numRows={(subscribers || []).length}
        >{renderColumns}</Table>
        <Dialog
          style={{ minWidth: 1366, padding: 0 }}
          icon="info-sign"
          title="New Message"
          isOpen={this.state.isOpen}
          onClose={() => {
            this.setState({
              isOpen: false,
            })
          }}
        ><Create deviceId={this.state.activeSession}/>
        </Dialog>
      </>
    );
  }
}