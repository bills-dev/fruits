
export interface State {
    app: {
        status: 'loading' | 'connected'
        activeStep: number
        callStatus: string
        remoteDeviceId: string
      subscribed: boolean
    },
    query:any,
    form: any
}