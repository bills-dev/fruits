import React                from 'react';
import {Component}          from 'react';
import {RefObject}          from 'react';
import {connect}            from 'react-redux';
import {bindActionCreators} from 'redux'
import {Theme}              from '@material-ui/core';
import {withStyles}         from '@material-ui/core';
import {WithStyles}         from '@material-ui/core';
import {createStyles}       from '@material-ui/core';
import {CircularProgress}       from '@material-ui/core';
import {Button}             from '@material-ui/core';
import {socket}             from "../../services/Socket";
import {changeCallStatus}   from "../../store/actions/app";
import {trigger}            from "../../store/actions/socket";
import {State}              from "../../types/State";
import green                from '@material-ui/core/colors/green';
import purple                from '@material-ui/core/colors/purple';
import AudioAnalyser from "./AudioAnalyser";
import {StreamMixer} from "./StreamMixer";


const peerConnectionConfig :any= {
    'iceServers': [
        {
            'url': 'stun:stun.l.google.com:19302'
        },
        {
            'url': 'turn:192.158.29.39:3478?transport=udp',
            'credential': 'JZEOEt2V3Qb0y27GRntt2u2PAYA=',
            'username': '28224511:1379330808'
        },
        {
            'url': 'turn:192.158.29.39:3478?transport=tcp',
            'credential': 'JZEOEt2V3Qb0y27GRntt2u2PAYA=',
            'username': '28224511:1379330808'
        }
    ]
};

const styles = (theme: Theme) => createStyles({
    main: {
        width: 'auto',
        display: 'block', // Fix IE 11 issue.
        marginTop: theme.spacing.unit * 3,
        marginLeft: theme.spacing.unit * 3,
        marginRight: theme.spacing.unit * 3,
        [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
            width: 400,
            marginLeft: 'auto',
            marginRight: 'auto',
        },
    },
    button: {
        color: theme.palette.getContrastText(green[500]),
        backgroundColor: green[500],
        '&:hover': {
            backgroundColor: green[700],
        },
    },
    buttonRed: {
        color: theme.palette.getContrastText(purple[500]),
        backgroundColor: purple[500],
        '&:hover': {
            backgroundColor: purple[700],
        },
    }

});

export interface CallerProps extends WithStyles<typeof styles> {
    changeCallStatus: typeof changeCallStatus
    callStatus:string
}

class Caller extends Component<CallerProps> {
    connection: RTCPeerConnection;
    videoRef: RefObject<any>;
    audioRef: RefObject<any>;
    stream:any;
    audioStream: any;

    streamMixer:StreamMixer;

    state = {
        callStatus: 'N/A',
        remoteDeviceId: 'N/A',
        incomingAudio: null
    };

    constructor(props) {
        super(props);
        this.streamMixer = new StreamMixer();
        window['caller'] = this;
        this.videoRef = React.createRef();
        this.audioRef = React.createRef();
    }

    componentDidMount(){
        socket.onMessage.attach((event) => {
            let message = JSON.parse(event.data);
            switch (message.event) {
                case 'call-status':
                    if(!message.data.callStatus){
                        this.stopPeerConnection();
                        this.setState({incomingAudio:null})
                    }
                    break;
                case 'answer':
                    console.log("ANSWER");
                    this.handleAnswer(message.data);
                    break;
                case 'sdp':
                    this.gotSdp(message.data);
                    break;
                default:
                    break
            }
        })
    }


    get remoteDeviceId() {
        return this.state.remoteDeviceId;
    }

    getDisplayStream() {
        return new Promise((resolve, reject) => {
            let displayMediaStreamConstraints = {
                video: true // currently you need to set {true} on Chrome
            };
            const mediaDevices: any = navigator.mediaDevices;
            mediaDevices.getDisplayMedia(displayMediaStreamConstraints)
                .then(resolve)
                .catch(reject);
        })

    }

    handleAnswer({ callStatus, sessionId: remoteDeviceId }) {
        this.setState({ callStatus, remoteDeviceId })
        if (callStatus == 'CONNECTING') {
            this.startCall()
        }
    };

    initiateCall = async ()=> {

        let stream = this.stream = await this.getDisplayStream();
        let audioStream = this.audioStream = await this.getAudioStream();
     //   this.streamMixer.addStream(stream);
        let st = { callStatus: "CALLING" };
        socket.emit('call-status', st)
    };

    stopCall = ()=> {
        this.stopPeerConnection();
        let st = { callStatus: null };
        socket.emit('call-status', st)
    };

    sendIceCondidate = ({ candidate })=>{
        candidate && socket.emit('sdp', { ice: candidate, sessionId: this.remoteDeviceId })
    };

    gotSdp = async (signal) => {
        if (signal.sdp) {
            await this.connection.setRemoteDescription(new RTCSessionDescription(signal.sdp));
            if (signal.sdp.type == 'offer') {
                const answer = await  this.connection.createAnswer();
                this.connection.setLocalDescription(answer);
                socket.emit('sdp', { sdp: this.connection.localDescription, sessionId: this.remoteDeviceId })

            }
        } else if (signal.ice) {
            this.connection.addIceCandidate(new RTCIceCandidate(signal.ice));
        }
    }

    getAudioStream() {
        return new Promise((resolve, reject) => {
            navigator.getUserMedia({
                video: false,
                audio: true
            }, (stream) => {
                resolve(stream);
            }, function (error) {
                console.error(error);
                reject("Camera capture failed!");
            });
        })
    };

    gotRemoteStreamTrack = (event) => {
        console.log('got remote streeam track');
        this.audioRef.current.setAttribute('autoplay', 'true');
        this.audioRef.current.srcObject = event.streams[0];
        this.setState({incomingAudio:event.streams[0]})
    };

    gotRemoteStream = (event) => {
        console.log('got remote streeam ');
        this.audioRef.current.setAttribute('autoplay', 'true');
        this.audioRef.current.srcObject = event.streams;
        this.setState({incomingAudio:event.streams})
    }

    oniceconnectionstatechange = (event) => {
        switch(this.connection.iceConnectionState) {
            case "closed":
            case "failed":
            case "disconnected":
                this.stopCall();
                break;
        }
    };

    onsignalingstatechange = (event) => {
        switch(this.connection.signalingState) {
            case "closed":
                this.stopCall();
                break;
        }
    };

    startPeerConnection = async () => {
        const peerConnection: any = this.connection = new RTCPeerConnection(peerConnectionConfig);
        peerConnection.onicecandidate = this.sendIceCondidate;
        peerConnection.oniceconnectionstatechange = this.oniceconnectionstatechange;
        peerConnection.onsignalingstatechange = this.onsignalingstatechange;
        peerConnection.ontrack = this.gotRemoteStreamTrack;
        peerConnection.onstream = this.gotRemoteStream;
        //ss
        peerConnection.addTrack(this.audioStream.getTracks()[0],this.audioStream);
        peerConnection.addTrack(this.stream.getTracks()[0],this.stream);
        //is caller
        let offer = await peerConnection.createOffer();
        await  peerConnection.setLocalDescription(offer);
        socket.emit('sdp', { sdp: peerConnection.localDescription, sessionId: this.remoteDeviceId })
    };

    stopPeerConnection = async () =>{
        console.log('stopPeerConnection', this.remoteDeviceId);
        const peerConnection: any = this.connection;
        if(peerConnection){
            peerConnection.onicecandidate = null;
            peerConnection.ontrack = null;
            peerConnection.onstream = null;
            this.videoRef.current.srcObject =null;
            peerConnection.close();
        }
        if(this.stream){
            this.stream.getTracks()[0].stop();
        }
        return
    }

    async startCall() {
        await  this.startPeerConnection();
    }

    render() {
        const { classes } = this.props;

        return (
            <main className={classes.main}>
                { (this.props.callStatus === 'CONNECTING' ) &&
                    <Button
                        className={classes.button}
                        disabled
                        fullWidth={true} color="inherit">

                        <CircularProgress disableShrink />
                    </Button>
                }

                { (this.props.callStatus === 'CALLING' ) &&
                    <Button
                        className={classes.button}
                        fullWidth={true} color="inherit" disabled>
                        <CircularProgress     size={24} disableShrink />
                    </Button>
                }
                {this.props.callStatus === 'IN_CALL' &&
                    <Button
                        className={classes.buttonRed}
                        onClick={this.stopCall}
                        fullWidth={true} color="inherit">
                        Stop Co-browsing
                    </Button>
                }
                { !this.props.callStatus  &&
                    <Button
                        className={classes.button}
                        onClick={this.initiateCall}
                        fullWidth={true} color="inherit">
                        Start Co-browsing
                    </Button>
                }
                {this.state.incomingAudio ? <AudioAnalyser audio={this.state.incomingAudio} /> : ''}
                <video ref={this.videoRef}/>
                <audio ref={this.audioRef}/>
            </main>
        );
    }
}

const mapStateToProps = (state: State) => ({
    callStatus:state.app.callStatus
});
const mapDispatchToProps = (dispatch) => (
    bindActionCreators({ changeCallStatus }, dispatch)
);
export default withStyles(styles)(connect(mapStateToProps, mapDispatchToProps)(Caller));