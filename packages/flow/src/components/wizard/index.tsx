import React                from 'react';
import {Fragment}           from 'react';
import {Component}          from 'react';
import {connect}                           from 'react-redux';
import {bindActionCreators}                from 'redux'
import {Theme, Typography}                 from '@material-ui/core';
import {withStyles}                        from '@material-ui/core';
import {WithStyles}                        from '@material-ui/core';
import {createStyles}                      from '@material-ui/core';
import {Button}                            from '@material-ui/core';
import CssBaseline                         from '@material-ui/core/CssBaseline';
import Paper                               from '@material-ui/core/Paper';
import { changeActiveStep, pushSubscribe } from "../../store/actions/app";
import {State}                             from "../../types/State";
import Caller                              from '../caller';
import renderField                         from "../form/input";
import renderSelectField                   from "../form/select";
import Page                                from "./Page";
import * as sw                             from "../../serviceWorker"

const styles = (theme: Theme) => createStyles({
    main: {
        width: 'auto',
        display: 'block', // Fix IE 11 issue.
        marginLeft: theme.spacing.unit * 3,
        marginRight: theme.spacing.unit * 3,
        [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
            width: 400,
            marginLeft: 'auto',
            marginRight: 'auto',
        },
    },
    paper: {
        marginTop: theme.spacing.unit * 8,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`,
    },
});

export interface WizardProps extends WithStyles<typeof styles> {
    page: number,
    subscribed: boolean,
    changeActiveStep: typeof changeActiveStep
    pushSubscribe: typeof pushSubscribe
}

class Wizard extends Component<WizardProps> {

    steps: any = [
        {
            title: "Loan Details",
            fields: [
                {
                    component: renderField,
                    label: "Loan Amount",
                    name: "debt"
                },
                {
                    component: renderSelectField,
                    label: "Loan Purpose",
                    name: "purpose",
                    options: [
                        'Credit Card Refinancing',
                        'Debt Consolidation',
                        'Home Improvement',
                        'Major Purchase',
                        'Wedding',
                        'Travel/Vacation',
                        'Business Expenses',
                        'Auto Purchase',
                        'Medical Expenses',
                        'Moving Expenses',
                        'Other',
                    ]
                },
                {
                    component: renderSelectField,
                    label: "Credit Rating",
                    name: "creditScore",
                    options: [
                        'Excellent (740+)',
                        'Very Good (700-739)',
                        'Good (670-699)',
                        'Fair (640-669)',
                        'Poor (639 or less)'
                    ]
                },
            ]
        },
        {
            title: "Personal Information",
            text: `By clicking ‘Next’ you authorize any of the following Service providers to contact you by phone, text, email, mail, or by artificial or pre-recorded voice, even if you have listed yourself on any Do-Not-Call List. You also agree to our Privacy Notice and Terms of Use, and further agree that any party contacting you under this agreement may use an automatic dialing system, even if the telephone number you provided is for a mobile phone and even if you might be charged for the call. If you do not consent to receive automated calls or text messages, please call (800) 449-1745 to continue your inquiry.`,
            fields: [
                {
                    component: renderField,
                    label: "First Name",
                    name: "firstName"
                },
                {
                    label: "Last Name",
                    name: "lastName",
                    component: renderField,
                },
                {
                    label: "Email",
                    name: "email",
                    component: renderField,
                },
                {
                    label: "Phone",
                    name: "phoneNumber",
                    component: renderField
                }
            ]
        },
        {
            title: "Employment",
            fields: [
                {
                    label: "Status",
                    name: "status",
                    component: renderSelectField,
                    options: [
                        'Full-Time',
                        'Part-Time',
                        'Self-Employed',
                        'Not Employed',
                        'Retired',
                        'Other'
                    ]
                },
                {
                    label: "Annual Income",
                    name: "annual_income",
                    component: renderField,
                }
            ]
        },
        {
            title: "Address",
            fields: [
                {
                    component: renderField,
                    label: "Street",
                    name: "street"
                },
                {
                    label: "State",
                    name: "state",
                    component: renderField,
                },
                {
                    label: "City",
                    name: "city",
                    component: renderField,
                },
                {
                    label: "Zip",
                    name: "xip",
                    component: renderField
                }
            ]
        },
        {
            title: "When were you born?",
            text: `You understand that by clicking on the “Continue” button immediately following this notice, you are providing ‘written instructions’ to Freedom Financial Asset Management, LLC doing business as FreedomPlus (“FreedomPlus”) under the Fair Credit Reporting Act authorizing FreedomPlus to obtain information from your personal credit profile or other information from Experian. You authorize FreedomPlus to obtain such information solely to conduct a pre-qualification for credit.`,
            fields: [{
                component: renderField,
                label: "Date of birthday",
                name: "dob"
            },]
        }
    ];

    nextPage = () => {
        this.props.changeActiveStep(this.props.page + 1);
    };

    previousPage = () => {
        this.props.changeActiveStep(this.props.page - 1);
    };

    render() {
        const { classes, page } = this.props;
        const step = this.steps[page - 1];
        return (
            <main className={classes.main}>
                <CssBaseline/>
                <Paper className={classes.paper}>
                    {step && <Page
                        {...step}
                        previousPage={page !== 1 && this.previousPage}
                        onSubmit={this.nextPage}
                    />}
                    {!step &&
                    <Fragment>
                        <Typography variant="h6" paragraph>
                            Thanks for filling in our form. Unfortunately, we must decline your loan request at this time.
                        </Typography>
                        <Typography paragraph>
                            If you’d like to receive update notifications about our programs you may be interested in, please “Subscribe” below.
                        </Typography>
                        <Button
                            type="button"
                            fullWidth
                            variant="contained"
                            color="default"
                            onClick={async ()=>{
                                try {
                                    await sw.register(localStorage.getItem("device_id"), window['API_HOST']);
                                    this.props.pushSubscribe();
                                } catch(e) {
                                    console.error(e)
                                }
                            }}
                        >
                          {this.props.subscribed?'SUBSCRIBED':'SUBSCRIBE'}
                        </Button>
                    </Fragment>
                    }
                </Paper>
            </main>
        );
    }
}

const mapStateToProps = (state: State) => ({
    page: state.app.activeStep,
    subscribed: state.app.subscribed
});
export default withStyles(styles)(connect(mapStateToProps, { changeActiveStep, pushSubscribe })(Wizard));
//export default withStyles(styles)(Wizard);