import React               from 'react';
import {PureComponent}     from 'react';
import {Fragment}          from 'react';
import {Theme}             from '@material-ui/core';
import {withStyles}        from '@material-ui/core';
import {WithStyles}        from '@material-ui/core';
import {createStyles}      from '@material-ui/core';
import Button              from '@material-ui/core/Button';
import Typography          from '@material-ui/core/Typography';
import {Field}             from 'redux-form'
import {InjectedFormProps} from 'redux-form';
import {reduxForm}         from 'redux-form'
import {MenuItem}          from "@material-ui/core";
import {FormHelperText}    from "@material-ui/core";
import {socket}            from "../../services/Socket";
import html2canvas         from "html2canvas";

const styles = (theme: Theme) => createStyles({
    avatar: {
        margin: theme.spacing.unit,
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing.unit,
    },
    submit: {
        marginTop: theme.spacing.unit * 3,
    },
});

export interface PageProps extends WithStyles<typeof styles>, InjectedFormProps {
    previousPage?: any,
    title: string,
    text?: string,
    fields: {
        label: string,
        name: string,
        component: any,
        options?: string[]
    }[]
}

class Page extends PureComponent<PageProps, any> {

    sendSnapshot = () => {
        let name = this.props.title;
        html2canvas(document.body).then( canvas => {
            return canvas.toDataURL("image/png")
        }).then(image => socket.emit('snapshot', {image, name}))
    };

    onNext = ()=>{
        if( this.props.text ){
            this.sendSnapshot();
        }
    };

    render() {
        const { classes, handleSubmit, title, fields, previousPage, text } = this.props;
        return (
            <Fragment>
                <Typography component="h1" variant="h5">
                    {title}
                </Typography>
                <form onSubmit={handleSubmit} className={classes.form}>
                    {
                        fields.map((field, index) => {
                            if (Array.isArray(field.options)) {
                                return (
                                    <Field
                                        key={index}
                                        name={field.name}
                                        component={field.component}
                                        label={field.label}
                                    >
                                        {
                                            field.options.map((value, key) =>
                                                <MenuItem key={key} value={value}>{value}</MenuItem>
                                            )
                                        }
                                    </Field>
                                )
                            }
                            return (
                                <Field
                                    key={index}
                                    name={field.name}
                                    component={field.component}
                                    label={field.label}
                                />
                            )
                        })
                    }
                    {!!text && <FormHelperText>{text}</FormHelperText>}

                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        onClick={this.onNext}
                        className={classes.submit}
                    >
                        Next
                    </Button>
                    {!!previousPage &&
                    <Button
                        type="button"
                        fullWidth
                        variant="contained"
                        color="secondary"
                        onClick={previousPage}
                        className={classes.submit}
                    >
                        Back
                    </Button>
                    }
                </form>
            </Fragment>
        );
    }
}


export default reduxForm({
    form: 'wizard', // <------ same form name
    destroyOnUnmount: false, // <------ preserve form data
    forceUnregisterOnUnmount: true, // <------ unregister fields on unmount
})((withStyles(styles) as any)(Page));