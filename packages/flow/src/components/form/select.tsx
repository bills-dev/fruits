import React                from 'react'
import {Select, withStyles} from '@material-ui/core';
import {MenuItem}           from '@material-ui/core';
import {FormControl}        from '@material-ui/core';
import {InputLabel}        from '@material-ui/core';
import {createStyles}      from '@material-ui/core';

const styles = theme => createStyles({
    formControl: {
        marginTop: theme.spacing.unit,
        minWidth: 120,
    }
});

const renderSelectField = ({input, label, meta: { touched, error }, children,classes, ...custom }) => {
    return (
        <FormControl fullWidth={true} className={classes.formControl}>
            <InputLabel>{label}</InputLabel>
            <Select
                style={{
                    width: "100%"
                }}
                {...input}
                onChange={(event) => input.onChange(event.target.value) }
                {...custom}
            >
                <MenuItem value="">
                    <em>None</em>
                </MenuItem>
                {children}
            </Select>
        </FormControl>

    )
};

export default withStyles(styles)(renderSelectField)