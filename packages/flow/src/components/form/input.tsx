import React, {PureComponent, useEffect} from 'react'
import {FunctionComponent}               from 'react'
import {TextField}                       from '@material-ui/core';
import {WrappedFieldProps}               from 'redux-form';
import {connect}                         from "react-redux";
import {isWatcher}                       from "../../store/selectors/app";
import {State}                           from "../../types/State";
// FunctionComponent<WrappedFieldProps> |

class InputField extends PureComponent<any> {
    inputRef;

    componentWillReceiveProps({ isWatcher, meta: { touched, error, visited } }) {
        if (isWatcher && this.inputRef && visited) {
            this.inputRef.focus();
        }
    }

    render() {
        const { isWatcher, input, meta: { touched, error, visited }, ...custom } = this.props;
        const adminProps = {} as any;
        if (isWatcher) {
            adminProps.onFocus = ()=>{}
        }
        return (
            <TextField
                margin="normal"
                inputRef={(input) => this.inputRef = input}
                fullWidth={true}
                error={touched && !!error}
                helperText={touched && error}
                {...input}
                {...custom}
                {...adminProps}
            >
            </TextField>
        )
    }
}


export default (connect((state: State) => ({
    isWatcher: isWatcher(state)
}),{})(InputField) as any)