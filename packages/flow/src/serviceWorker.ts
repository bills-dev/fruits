function urlBase64ToUint8Array(base64String) {
  const padding = '='.repeat((4 - base64String.length % 4) % 4);
  const base64 = (base64String + padding)
    .replace(/\-/g, '+')
    .replace(/_/g, '/');

  const rawData = window.atob(base64);
  const outputArray = new Uint8Array(rawData.length);

  for (let i = 0; i < rawData.length; ++i) {
    outputArray[i] = rawData.charCodeAt(i);
  }

  return outputArray;
}

export async function register(deviceId, apiEndpoint) {
  if ('serviceWorker' in navigator) {
    // The URL constructor is available in all browsers that support SW.
    const publicUrl = new URL(
      (process as { env: { [key: string]: string } }).env.PUBLIC_URL,
      window.location.href
    );
    if (publicUrl.origin !== window.location.origin) {
      // Our service worker won't work if PUBLIC_URL is on a different origin
      // from what our page is served on. This might happen if a CDN is used to
      // serve assets; see https://github.com/facebook/create-react-app/issues/2374
      return;
    }

    const swUrl = `${process.env.PUBLIC_URL}/service-worker-custom.js`;
    try {
      await navigator.serviceWorker.register(swUrl);
      const result = await Notification.requestPermission();
      if (result === 'granted') {
        const registration = await navigator.serviceWorker.ready;
        const subscription = await registration.pushManager.getSubscription();

        if (subscription) {
          await fetch(`${apiEndpoint}/api/notification/subscribe`, {
            method : 'post',
            headers: {
              'Content-type': 'application/json'
            },
            body   : JSON.stringify({
              deviceId    : deviceId,
              subscription: subscription,
            })
          })
        } else {
          const response = await fetch(`${apiEndpoint}/api/notification/get-public-key`);
          const responseJson = await response.json();

          const subscription = await registration.pushManager.subscribe({
            userVisibleOnly     : true,
            applicationServerKey: urlBase64ToUint8Array(responseJson.publicKey),
          });

          await fetch(`${apiEndpoint}/api/notification/subscribe`, {
            method : 'post',
            headers: {
              'Content-type': 'application/json'
            },
            body   : JSON.stringify({
              deviceId    : deviceId,
              subscription: subscription,
            }),
          });
        }

        return true
      }
    } catch (e) {
      console.error('Something went wrong:', e);
      return false
    }
  }
}