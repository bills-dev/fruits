import {stringify} from 'querystring';

export class HttpClient {
    public async request(path: string, options: any = {}, query?: any) {
        if (query) {
            path = `${path}?${stringify(query)}`;
        }
        if (!options.headers) {
            options.headers = {};
        }
        if (options.body) {
            options.headers = Object.assign({
                'Content-Type': 'application/json',
            }, options.headers);
            if (options.headers['Content-Type'] === 'application/json') {
                options.body = JSON.stringify(options.body);
            }
        }
        const res = await fetch(path, options);
        let body;
        const contentType = res.headers.get('Content-Type');
        if (contentType.match(/application\/json/)) {
            try {
                body = await res.json();
            } catch (e) {
                body = '';
            }
        } else {
            body = await res.text();
        }
        const response = {
            status: res.status,
            body,
        };
        if (response.status < 400) {
            return response;
        } else {
            throw response;
        }
    }

    public get(path: string, options: any = {}, query?) {
        options.method = 'GET';
        return this.request(path, options, query);
    }

    public post(path: string, options: any = {}, query?) {
        options.method = 'POST';
        return this.request(path, options, query);
    }
}