import {ActionType}   from "../actions/ActionType";
import {initialState} from "../index";


export default (state = initialState.app, action) => {
    switch (action.type) {
        case ActionType.INIT :
            return {
                ...state,
                status: "connected"
            };
        case ActionType.CHANGE_ACTIVE_STEP:
            return { ...state, ...action.payload };
        case ActionType.CHANGE_CALL_STATUS:
            return { ...state, ...action.payload };
        case ActionType.CHANGE_REMOTE_DEVICE_ID:
            return { ...state, ...action.payload };
      case ActionType.PUSH_SUBSCRIBE:
          return {...state, subscribed: true};
        default:
            return state
    }
}