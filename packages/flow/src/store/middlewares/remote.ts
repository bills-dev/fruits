import {socket}           from "../../services/Socket";
import {runActions} from "../actions/app";
import {isWatcher}        from "../selectors/app";

export function createRemoteSocketMiddleware() {
    return ({ getState, dispatch }) => next => {
        if (isWatcher(getState())) {
            socket.onMessage.attach((e) => {
                let { data, event } = JSON.parse(e.data);
                switch (event) {
                    case 'welcome':
                        if (!data.active) {
                            dispatch(runActions(data.actions))
                        }else{
                            dispatch(runActions(data.actions))
                        }
                        break;
                    case 'redux-action':
                        dispatch(data.action);
                        break;
                    default:
                        break
                }
            });
            return (action) => {

                return next(action);
            };
        }
        return action => {
            return next(action);
        }
    }
}