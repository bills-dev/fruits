import thunk                  from "redux-thunk";
import {ThunkAction as Thunk} from "redux-thunk";
import {Action}               from "redux";
import {State}                from "../../types/State";
import {Api}                  from "../../services/Api";

export type ThunkAction<T = any, R= void> = Thunk<R, State, Api, Action<T>>

declare module "redux" {
    export interface Dispatch<A> {
        <R>(asyncAction: ThunkAction<A,R>): R;
    }
}


export default thunk;