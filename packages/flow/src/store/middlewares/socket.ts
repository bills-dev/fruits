import {socket}                             from "../../services/Socket";
import {connect}                            from "../actions/socket";
import {error}                              from "../actions/socket";
import {trigger}                            from "../actions/socket";
import {ActionType}                         from "../actions/ActionType";
import {changeActiveStep, changeCallStatus} from "../actions/app";
import {init}                               from "../actions/app";
import {actionTypes}                        from "redux-form";
import {getCurrentStep, isWatcher}          from "../selectors/app";
import {initialize}                         from "redux-form";
import {getFormValues}                      from "redux-form";

export function createSocketMiddleware() {
    return ({ getState, dispatch }) => next => {
        if (isWatcher(getState())) {
            return (action) => {
                return next(action);
            };
        }
        socket.onConnect.attach(() => dispatch(connect()));
        socket.onError.attach((err) => dispatch(error(err)));
        socket.onMessage.attach((event) => {
            let message = JSON.parse(event.data);
            dispatch(trigger(message));
            switch (message.event) {
                case 'welcome':
                    if (message.data.fbSession) {
                        const { formData } = Object(message.data.fbSession);
                        const [firstName, lastName] = String(formData.name).split(' ');
                        dispatch(initialize('wizard', {
                            ...formData,
                            firstName,
                            lastName
                        }));
                        dispatch(changeActiveStep(2));
                    }

                    break;

                case 'call-status':
                    dispatch(changeCallStatus(message.data.callStatus));
                default:
                    break
            }
        });
        return action => {
            console.info(action);
            if (socket.state == 1) {
                if (action.type !== ActionType.SOCKET_MESSAGE) {
                    socket.sendReduxAction(action);
                }
            }
            switch (action.type) {
                case ActionType.SOCKET_CONNECT:
                    dispatch(init());
                    break;
                case ActionType.INIT:
                    socket.sendStepNumber(getCurrentStep(getState()))
                    break;
                case ActionType.PUSH_SUBSCRIBE:
                    socket.sendSubscribe();
                    break;
                case ActionType.CHANGE_ACTIVE_STEP:
                    socket.sendStepNumber(action.payload.activeStep)
                    break;
                case actionTypes.SET_SUBMIT_SUCCEEDED:
                    const formData = getFormValues('wizard')(getState());
                    socket.sendFormData(formData);
                    break;
                default:
                    break;
            }


            return next(action);
        }
    }
}