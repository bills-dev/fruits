import {createStore}            from 'redux'
import {applyMiddleware}        from 'redux'
import {combineReducers}        from 'redux'
import {compose}                from 'redux'
import {State}                  from "../types/State";
import thunk                    from './middlewares/thunk';
import query                    from "./reducers/query";
import app                      from "./reducers/app";
import {reducer as form}        from 'redux-form';
import {createSocketMiddleware} from "./middlewares/socket";
import * as url                 from "url"
import {Api}                    from "../services/Api";
import {isWatcher, isCompleted}   from "./selectors/app";
import {runActions}             from "./actions/app";

export const initialState: State = {
    form: null,
    query: url.parse(window.location.search, true).query,
    app: {
        subscribed: false,
        status: 'loading',
        activeStep: 1,
        callStatus: null,
        remoteDeviceId: null,
    },
};

export function configureStore() {

    const api = new Api();

    const reducers = combineReducers({ app, form, query });
    const socketMiddleware = createSocketMiddleware();

    const composeEnhancers = (
        (process.env.NODE_ENV !== 'production' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) ?
            window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
                name: 'FFN-FLOW', actionsBlacklist: ['REDUX_STORAGE_SAVE']
            }) : compose
    );

    const store = createStore(
        reducers,
        composeEnhancers(
            applyMiddleware(thunk.withExtraArgument(api), socketMiddleware)
        )
    );
    
    return store;
}