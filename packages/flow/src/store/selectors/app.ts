import {createSelector} from "reselect";
import {State}          from "../../types/State";

export const getState = (state:State) => state;
export const getQuery = (state:State) => state.query;
export const getSid = createSelector(
    getQuery,
    (query)=>query.sid
);
export const isUser = createSelector(
    getQuery,
    (query)=>!!query.type
);
export const isWatcher = createSelector(
    getQuery,
    (query)=>query.type ==='watcher'
);
export const isCompleted = createSelector(
    getQuery,
    (query)=>query.status ==='completed'
);

export const getCurrentStep = createSelector(
    getState,
    (state)=>state.app.activeStep
);