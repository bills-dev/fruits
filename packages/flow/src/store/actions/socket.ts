import {ActionType} from "./ActionType";

export const connect = ()=>({
    type:ActionType.SOCKET_CONNECT
});

export const error = (message)=>({
    type:ActionType.SOCKET_ERROR,
    payload:message
});

export const trigger = (message)=>({
    type:ActionType.SOCKET_MESSAGE,
    payload:message
});
