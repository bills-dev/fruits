import {ActionType}  from "./ActionType";
import {ThunkAction} from "../middlewares/thunk";
import {actionTypes} from "redux-form";
import {getSid}      from "../selectors/app";


export const init = () => ({
    type: ActionType.INIT
});

export const runActions = (actions = [],wait: boolean = false): ThunkAction => {
    return async (dispatch, getState, api) => {
        try {
            if (actions.length) {
                for(const a of actions) {
                    dispatch(a.action);
                    //( <any>document.querySelector(`input[name = ${a.meta.field}]`)).focus()
                }
            }
        } catch (e) {
            console.error(e);
        }
    }
};


export interface ChangeActiveStepAction {
    type: ActionType.CHANGE_ACTIVE_STEP,
    payload?: { activeStep: number }
}

export const changeActiveStep = (pageNumber: number): ChangeActiveStepAction => ({
    type: ActionType.CHANGE_ACTIVE_STEP,
    payload: { activeStep: pageNumber }
});
export const changeCallStatus = (callStatus)=>({
    type:ActionType.CHANGE_CALL_STATUS,
    payload:{callStatus}
});
export const pushSubscribe = ()=>({
    type:ActionType.PUSH_SUBSCRIBE,
});
export const changeRemoteDeviceId = (remoteDeviceId)=>({
    type:ActionType.CHANGE_REMOTE_DEVICE_ID,
    payload:{remoteDeviceId}
});