import {applyMiddleware, compose, createStore} from 'redux'
import {combineReducers}                       from 'redux'
import query                                   from "./reducers/query";
import app                                     from "./reducers/app";
import {reducer as form}                       from 'redux-form';
import {createRemoteSocketMiddleware}          from "./middlewares/remote";
import thunk                                   from "./middlewares/thunk";

export function configureRemoteStore(DevTool) {
    const reducers = combineReducers({ app, form, query, });
    const remoteSocketMiddleware = createRemoteSocketMiddleware();
    return createStore(
        reducers,
        compose(
            applyMiddleware(thunk,remoteSocketMiddleware),
            DevTool.instrument()
        )
    )
}