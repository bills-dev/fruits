import './index.css';
import React                  from 'react';
import {render}               from 'react-dom';
import App                    from './App';
import {configureStore}       from "./store";
import {Provider}             from "react-redux";
import {createMuiTheme}       from '@material-ui/core/styles';
import {MuiThemeProvider}     from '@material-ui/core/styles';
import {isWatcher}            from "./store/selectors/app";
import {configureRemoteStore} from "./store/remote";
import SliderMonitor          from 'redux-slider-monitor';
import {createDevTools}       from 'redux-devtools';


export const DevTool = createDevTools(
    <SliderMonitor hideResetButton={true}/>
);

const renderBackdrop = () =>{
    const backdrop = document.createElement('div');
    backdrop.id = 'backdrop';
    document.body.prepend(backdrop);
};

async function main() {
    const store = configureStore();
    const remoteStore = configureRemoteStore(DevTool);
    const theme = createMuiTheme({
        // palette: {
        //     primary: {
        //         light: "#727985",
        //         main: "#272A34",
        //         dark: "#473D42",
        //     },
        //     secondary: {
        //         light: "#B5B2AF",
        //         main: "#9E5C52",
        //         dark: "#889F85",
        //     },
        // },
        typography: {
            useNextVariants: true,
        }
    });

    if (!isWatcher(store.getState())) {
        render(
            <Provider store={store}>
                <MuiThemeProvider theme={theme}>
                    <App/>
                </MuiThemeProvider>
            </Provider>,
            document.getElementById('root')
        );
    } else {
        render(
            <Provider store={remoteStore}>
                <MuiThemeProvider theme={theme}>
                    <App/>
                </MuiThemeProvider>
            </Provider>,
            document.getElementById('root')
        );
        render(
            <Provider store={remoteStore}>
                <DevTool />
            </Provider>,
            document.getElementById('control')
        );
        renderBackdrop()
    }
}

main().catch(console.error);

