import React            from 'react';
import {PureComponent}  from 'react';
import {Fragment}       from 'react';
import './App.css';
import {connect}        from "react-redux";
import {LinearProgress} from '@material-ui/core';
import {State}          from "./types/State";
import Wizard           from "./components/wizard";
import {withStyles}     from '@material-ui/core/styles';
import {WithStyles}     from '@material-ui/core';
import AppBar           from '@material-ui/core/AppBar';
import Toolbar          from '@material-ui/core/Toolbar';
import Typography       from '@material-ui/core/Typography';
import Button           from '@material-ui/core/Button';
import IconButton       from '@material-ui/core/IconButton';
import Caller           from './components/caller';
import * as sW from "./serviceWorker"

export interface AppProps extends WithStyles<typeof styles> {
    loading: boolean
}

const styles = {
    root: {
        flexGrow: 1,
    },
    grow: {
        flexGrow: 1,
    },
    menuButton: {
        marginLeft: -12,
        marginRight: 20,
    },
};

class App extends PureComponent<AppProps> {

    render() {
        const {
            loading, classes
        } = this.props;

        return (

            <Fragment>
                <div className={classes.root}>
                    <AppBar position="static">
                        <Toolbar>
                            <IconButton className={classes.menuButton} color="inherit" aria-label="Menu">
                                FF
                            </IconButton>
                            <Typography variant="h6" color="inherit" className={classes.grow}>
                                Fruits Flow
                            </Typography>
                        </Toolbar>
                    </AppBar>
                </div>
                {loading && <LinearProgress color={"secondary"}/>}
                <Wizard/>
                <Caller/>
            </Fragment>
        );
    }
}

export default connect((state: State) => {
    return {
        loading: state.app.status === 'loading'
    }
})(withStyles(styles)(App));

