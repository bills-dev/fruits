var apiHost = 'http://127.0.0.1:3002';

self.addEventListener('install', function (event) {
  event.waitUntil(self.skipWaiting());
});

self.addEventListener('activate', function (event) {
  event.waitUntil(self.clients.claim());
});

self.addEventListener('push', function (event) {
  var options = {
    dir: "auto",
  };
  var nfData = event.data.json();

  if (nfData.body) {
    options.body = nfData.body;
  }
  if (nfData.icon) {
    options.icon = nfData.icon;
  }
  if (nfData.image) {
    options.image = nfData.image;
  }
  if (nfData.badge) {
    options.badge = nfData.badge;
  }
  if (nfData.vibrate) {
    options.vibrate = nfData.vibrate;
  }
  if (nfData.sound) {
    options.sound = nfData.sound;
  }
  if (nfData.tag) {
    options.tag = nfData.tag;
  }
  if (nfData.data) {
    options.data = nfData.data;
  }
  if (nfData.requireInteraction) {
    options.requireInteraction = nfData.requireInteraction;
  }
  if (nfData.renotify) {
    options.renotify = nfData.renotify;
  }
  if (nfData.silent) {
    options.silent = nfData.silent;
  }
  if (nfData.actions) {
    options.actions = nfData.actions;
  }
  if (nfData.timestamp) {
    options.timestamp = nfData.timestamp;
  }

  var title = "Notification";
  if (nfData.title) {
    title = nfData.title;
  }

  event.waitUntil(self.registration.showNotification(title, options));
});

self.addEventListener('notificationclick', function (event) {
  console.info(event.notification);
  var data = event.notification.data;
  var url = data['launchUrl'];

  event.notification.close();
  event.waitUntil(self.clients.matchAll({type: 'window'}).then(function () {
    if (self.clients.openWindow) {
      self.clients.openWindow(url)
        .catch(function (e) {
          console.error(e)
        });
    }
  }));

});

self.addEventListener('notificationclose', function (event) {
  var notification = event.notification;
  fetch(apiHost + "/api/notification/close", {
    method : "POST",
    headers: {
      'Content-type': 'application/json'
    },
    body   : JSON.stringify({
      deviceId: notification.tag
    })
  }).catch(function (error) {
    console.error(error)
  })
});
