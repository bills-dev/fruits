var Push = {
  apiHost              : 'http://127.0.0.1:3002',
  subscribeApi         : '/api/notification/subscribe',
  getPublicKeyApi      : '/api/notification/get-public-key',
  init                 : function () {
    if ('serviceWorker' in navigator) {
      navigator.serviceWorker.register('/service-worker.js')
        .then(function () {
          Notification.requestPermission().then(function (result) {
            if (result === 'granted') {
              navigator.serviceWorker.ready.then(function (registration) {
                registration.pushManager.getSubscription()
                  .then(function (sub) {
                    if (sub === null) {
                      fetch(Push.apiHost + Push.getPublicKeyApi)
                        .then(function (response) {
                          return response.json()
                        })
                        .then(function (responseJson) {
                          var convertedVapidKey = Push.urlBase64ToUint8Array(responseJson.publicKey);
                          registration.pushManager.subscribe({
                            userVisibleOnly     : true,
                            applicationServerKey: convertedVapidKey
                          }).then(function (subscription) {
                            fetch(Push.apiHost + Push.subscribeApi, {
                              method : 'post',
                              headers: {
                                'Content-type': 'application/json'
                              },
                              body   : JSON.stringify(subscription),
                            }).then(function (response) {
                              return response.json();
                            }).then(function (responseJson) {
                              console.info(responseJson)
                            }).catch(function (error) {
                              console.error(error);
                            });
                          })
                        })
                        .catch(function (error) {
                          console.error(error);
                        });
                    } else {
                      fetch(Push.apiHost + Push.subscribeApi, {
                        method : 'post',
                        headers: {
                          'Content-type': 'application/json'
                        },
                        body   : JSON.stringify(sub),
                      }).then(function (response) {
                        return response.json();
                      }).then(function (responseJson) {
                        console.info(responseJson)
                      }).catch(function (error) {
                        console.error(error);
                      });
                    }
                  });
              });
            }
          });
        })
        .catch(function (error) {
          console.log('Service worker registration failed, error:', error);
        });
    }
  },
  urlBase64ToUint8Array: function (base64String) {
    var padding = '='.repeat((4 - base64String.length % 4) % 4);
    var base64 = (base64String + padding)
      .replace(/\-/g, '+')
      .replace(/_/g, '/');

    var rawData = window.atob(base64);
    var outputArray = new Uint8Array(rawData.length);

    for (var i = 0; i < rawData.length; ++i) {
      outputArray[i] = rawData.charCodeAt(i);
    }
    return outputArray;
  },
};